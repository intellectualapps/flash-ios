//
//  BaseViewProtocol.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/11/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BaseViewProtocol <NSObject>

- (NSString *)viewIdentifier;
- (NSString *)viewStateIdentifier;
- (void)setViewIdentifier:(NSString *)viewIdentifier;
- (void)setViewStateIdentifier:(NSString *)viewStateIdentifier;
- (void)willBecomeRecycled;

@end