//
//  Utils.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/3/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "Utils.h"
#import "UIImage+Resize.h"
#import "KeychainItemWrapper.h"
#import <map>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



CGFloat RetinaPixel = 0.5f;
static std::map<int, CTFontRef> fontCache;
static __SYNCHRONIZED_DEFINE(fontCache) = PTHREAD_MUTEX_INITIALIZER;

CGFloat RetinaFloor(CGFloat value) {
    return IsRetina() ? (CGFloor(value * 2.0f)) / 2.0f : CGFloor(value);
}

bool IsArabic() {
    static bool value = false;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
                      value = [language isEqualToString:@"ar"];
                  });
    return value;
}

CTFontRef CoreTextFontOfNameAndSize(CGFloat size,  BOOL isBold) {
    int key = (int)(size * 2.0f);
    CTFontRef result = NULL;
    
    __SYNCHRONIZED_BEGIN(fontCache);
    auto it = fontCache.find(key);
    if (it != fontCache.end()) {
        result = it->second;
    } else {
        if (isBold) {
            result = CTFontCreateWithName(CFSTR("Quicksand-Bold"), floorf(size * 2.0f) / 2.0f, NULL);
        } else {
            result = CTFontCreateWithName(CFSTR("Quicksand-Regular"), floorf(size * 2.0f) / 2.0f, NULL);
        }
        fontCache[key] = result;
    }
    __SYNCHRONIZED_END(fontCache);
    
    return result;
}

UIColor *AccentColor() {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = UIColorRGB(0x007ee5);
    });
    return color;
}

UIImage *FixOrientationAndCrop(UIImage *source, CGRect cropFrame, CGSize imageSize) {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imageSize.width, imageSize.height), true, 1.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGSize sourceSize = source.size;
    float sourceScale = source.scale;
    sourceSize.width *= sourceScale;
    sourceSize.height *= sourceScale;
    
    CGContextScaleCTM (context, imageSize.width / cropFrame.size.width, imageSize.height / cropFrame.size.height);
    [source drawAtPoint:CGPointMake(-cropFrame.origin.x, -cropFrame.origin.y) blendMode:kCGBlendModeCopy alpha:1.0f];
    //[source drawInRect:CGRectMake(-cropFrame.origin.x, -cropFrame.origin.y, sourceSize.width, sourceSize.height) blendMode:kCGBlendModeCopy alpha:1.0f];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

bool IsRetina() {
    static bool value = true;
    static bool initialized = false;
    if (!initialized) {
        value = [[UIScreen mainScreen] scale] > 1.5f;
        initialized = true;
        
        RetinaPixel = value ? 0.5f : 0.0f;
    }
    return value;
}

int cpuCoreCount() {
    static int count = 0;
    if (count == 0) {
        size_t len;
        unsigned int ncpu;
        len = sizeof(ncpu);
        sysctlbyname("hw.ncpu", &ncpu, &len, NULL, 0);
        count = ncpu;
    }
    
    return count;
}

CGFloat ScreenScaling() {
    static CGFloat value = 2.0f;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        value = [UIScreen mainScreen].scale;
    });
    
    return value;
}

NSString *currentLanguage() {
    static NSString *language = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        language = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
    });
    
    return language;
}

NSString *appSortVersion() {
    static NSString *version = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    });
    
    return version;
}

int iosMajorVersion() {
    return [[[UIDevice currentDevice] systemVersion] intValue];
}

bool StringCompare(NSString *s1, NSString *s2) {
    if (s1.length == 0 && s2.length == 0)
        return true;
    if ((s1 == nil) != (s2 == nil))
        return false;
    
    return s1 == nil || [s1 isEqualToString:s2];
}

bool IsPad() {
    static bool value = false;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      value = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
                  });
    
    return value;
}

UIFont *SystemFontOfSize(CGFloat size) {
    
    static bool useSystem = false;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        useSystem = iosMajorVersion() >= 7;
    });
    
    if (useSystem)
        return [UIFont boldSystemFontOfSize:size];
    else
        return [UIFont fontWithName:@"HelveticaNeue" size:size];
}

CGSize ScreenSize() {
    static CGSize size;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIScreen *screen = [UIScreen mainScreen];
        if ([screen respondsToSelector:@selector(coordinateSpace)])
        {
            size = [screen.coordinateSpace convertRect:screen.bounds toCoordinateSpace:screen.fixedCoordinateSpace].size;
        }else{
            size = screen.bounds.size;
        }
    });
    
    return size;
}

UIFont *BoldSystemFontOfSize(CGFloat size) {
    static bool useSystem = false;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      useSystem = iosMajorVersion() >= 7;
                  });
    
    if (useSystem)
        return [UIFont boldSystemFontOfSize:size];
    else
        return [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
}

static inline NSString* monthFull(int month) {
    switch (month) {
        case 1: {
            return @"January";
        }
        case 2: {
            return @"February";
        }
        case 3: {
            return @"March";
        }
        case 4: {
            return @"April";
        }
        case 5: {
            return @"May";
        }
        case 6: {
            return @"June";
        }
        case 7: {
            return @"July";
        }
        case 8: {
            return @"August";
        }
        case 9: {
            return @"September";
        }
        case 10: {
            return @"October";
        }
        case 11: {
            return @"November";
        }
        case 12: {
            return @"December";
        }
    }
    return @"";
}

static inline NSString* wdayFull(int day) {
    switch (day) {
        case 0:
            return @"日"; // Sunday
            break;
        case 1:
            return @"月"; // Monday
            break;
        case 2:
            return @"火"; // Tuesday
            break;
        case 3:
            return @"水"; // Wednesday
            break;
        case 4:
            return @"木"; // Thursday
            break;
        case 5:
            return @"金"; // Friday
            break;
        case 6:
            return @"土"; // Saturday
            break;
            
            
        default:
            return @"";
            break;
    }
}


@implementation Utils
+ (NSString *)libaryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    if (paths.count > 0) {
        return [paths objectAtIndex:0];
    }
    return @"";
}

+ (CGSize) sizeForText:(NSString *)text
             boundSize:(CGSize)bsize
                option:(NSStringDrawingOptions)option
         lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font {
    if (!text || text.length ==0 || !font) {
        return CGSizeZero;
    }
    CGSize textSize = CGSizeZero;
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    textSize = [text boundingRectWithSize: bsize
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:font,NSParagraphStyleAttributeName:paragraphStyle}
                                  context:nil].size;
    return textSize;
}

/*
 "date.year" = "年";
 "date.month" = "月";
 "date.hour" = "時";
 "date.minue" = "分";
 */
+ (NSString *)monthAndYearWithTimeInterval:(NSTimeInterval)time {
    time_t t = time;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return  [NSString stringWithFormat:@"%04d %@ %d %@",
             timeinfo.tm_year,
             NSLocalizedString(@"date.year", ""),
             timeinfo.tm_mon,
             NSLocalizedString(@"date.month", "")
             ];
}

+ (NSString *)hourAndSecondWithTimeInterval:(long)time {
    time_t t = time;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return  [NSString stringWithFormat:@"%d %@ %d %@",
             timeinfo.tm_hour,
             NSLocalizedString(@"date.hour", ""),
             timeinfo.tm_min,
             NSLocalizedString(@"date.minue", "")
             ];
}

////New requirement: Only check count characters after dot
+ (BOOL)validateEmail:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{1,}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot =(int) aRange.location;
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            if(topLevelDomain != nil && topLevelDomain.length > 0) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

//U+30A0–U+30FF: Katakana
+ (BOOL)isKatanaValid:(NSString *)input {
    if (input) {
        input = [input stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        input = [input stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    NSCharacterSet *charSet = [[NSCharacterSet characterSetWithCharactersInString:KatakanaFullSizeRegexAccept] invertedSet];
    if ([input rangeOfCharacterFromSet:charSet].location == NSNotFound) {
        return YES;
    }
    charSet = [[NSCharacterSet characterSetWithCharactersInString:KatakanaHalftSizeRegexAccept] invertedSet];
    if ([input rangeOfCharacterFromSet:charSet].location == NSNotFound) {
        return YES;
    }
    return NO;
}

+ (NSInteger)numberOfAlphaBetInString:(NSString *)string {
    NSInteger count = 0;
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ((c >= 97 && c <= 122) || (c >= 65 && c <= 90)) {
            count += 1;
        }
    }
    return count;
}

+ (BOOL)allowsAppendString:(NSString *)string {
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"] ;
    for (NSUInteger i = 0; i < [string length]; ++i) {
        unichar uchar = [string characterAtIndex:i] ;
        if (![characterSet characterIsMember:uchar]) {
            return NO ;
        }
    }
    return YES ;
}


+ (UIImage *) cropImageCorrespondingWithCurrentDevice:(UIImage *)originImage {
    if (originImage) {
        NSData *jpgImageData = UIImageJPEGRepresentation(originImage, 1.0);
        if (jpgImageData){
            UIImage *jpgImage = [UIImage imageWithData:jpgImageData];
            CGSize newSize = originImage.size;
            if (newSize.width > 1080) {
                CGFloat percent = newSize.height / newSize.width;
                newSize.width = 1080;
                newSize.height = percent * newSize.width;
                jpgImage = [jpgImage resizedImage:newSize interpolationQuality:kCGInterpolationNone];
                return jpgImage;
            }
        }
    };
    return originImage;
}

+ (void)dispatchOnMainAsyncSafe:(dispatch_block_t)block {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}


+ (void) dispatchMainSyncSafe:(dispatch_block_t)block {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

+ (NSString *)getExpireMonthFromValidityPeriod:(NSString *)validityPeriod {
    if (validityPeriod && validityPeriod.length > 0) {
        NSRange range = [validityPeriod rangeOfString:@"/"];
        if (range.location != NSNotFound) {
            return [validityPeriod substringToIndex:range.location];
        }
    }
    return @"";
}

+ (void)saveAlertSettingCount:(NSInteger)count {
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:@"alertLocationSettingCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)formatAmount:(NSInteger)amount {
    if (amount >= 1000) {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.groupingSize = 3;
        formatter.usesGroupingSeparator = true;
        NSString *formatString = @"";
        formatString = [formatter stringFromNumber:[NSNumber numberWithInteger:amount]];
        return formatString;
    }
    return [NSString stringWithFormat:@"%ld",amount];
}

+ (NSString *)replaceString:(NSString *)origin withString:(NSString *)str range:(NSRange)range {
    return [origin stringByReplacingCharactersInRange:range withString:str];
}

+ (void)saveDeviceToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:(token) ? token : @"" forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)deviceToken {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceToken"];
}

+ (NSString *)deviceId {
    NSString *dvid = [KeychainItemWrapper load:@"cplDeviceUdid"];
    if (dvid != nil && dvid.length > 0) {
        return dvid;
    }
    dvid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    if (dvid != nil) {
        [KeychainItemWrapper save:@"cplDeviceUdid" data:dvid];
        return dvid;
    }
    return @"";
}

+ (NSString *)generateUUID {
    NSString *result = nil;
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    if (uuid) {
        result = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
    }
    return result;
}

+ (NSArray *)textCheckingResults:(NSString *)_text
{
    /*#ifdef DEBUG
     return nil;
     #endif*/
    
    if (_text.length < 3)
        return nil;
    
    bool containsSomething = false;
    
    const unichar *stringCharacters = CFStringGetCharactersPtr((__bridge CFStringRef)_text);
    int length = int(_text.length);
    
    int digitsInRow = 0;
    int schemeSequence = 0;
    int dotSequence = 0;
    
    unichar lastChar = 0;
    
    if (stringCharacters != NULL)
    {
        for (int i = 0; i < length; i++)
        {
            unichar c = stringCharacters[i];
            
            if (c >= '0' && c <= '9')
            {
                digitsInRow++;
                if (digitsInRow >= 6)
                {
                    containsSomething = true;
                    break;
                }
                
                schemeSequence = 0;
                dotSequence = 0;
            }
            else if (c == ':')
            {
                if (schemeSequence == 0)
                    schemeSequence = 1;
                else
                    schemeSequence = 0;
            }
            else if (c == '/')
            {
                if (schemeSequence == 2)
                {
                    containsSomething = true;
                    break;
                }
                
                if (schemeSequence == 1)
                    schemeSequence++;
                else
                    schemeSequence = 0;
            }
            else if (c == '.')
            {
                if (dotSequence == 0 && lastChar != ' ')
                    dotSequence++;
                else
                    dotSequence = 0;
            }
            else if (c != ' ' && lastChar == '.' && dotSequence == 1)
            {
                containsSomething = true;
                break;
            }
            
            lastChar = c;
        }
    }
    else
    {
        SEL sel = @selector(characterAtIndex:);
        unichar (*characterAtIndexImp)(id, SEL, NSUInteger) = (typeof(characterAtIndexImp))[_text methodForSelector:sel];
        
        for (int i = 0; i < length; i++)
        {
            unichar c = characterAtIndexImp(_text, sel, i);
            
            if (c >= '0' && c <= '9')
            {
                digitsInRow++;
                if (digitsInRow >= 6)
                {
                    containsSomething = true;
                    break;
                }
                
                schemeSequence = 0;
                dotSequence = 0;
            }
            else if (!(c != ' ' && digitsInRow > 0))
                digitsInRow = 0;
            
            if (c == ':')
            {
                if (schemeSequence == 0)
                    schemeSequence = 1;
                else
                    schemeSequence = 0;
            }
            else if (c == '/')
            {
                if (schemeSequence == 2)
                {
                    containsSomething = true;
                    break;
                }
                
                if (schemeSequence == 1)
                    schemeSequence++;
                else
                    schemeSequence = 0;
            }
            else if (c == '.')
            {
                if (dotSequence == 0 && lastChar != ' ')
                    dotSequence++;
                else
                    dotSequence = 0;
            }
            else if (c != ' ' && lastChar == '.' && dotSequence == 1)
            {
                containsSomething = true;
                break;
            }
            else
            {
                dotSequence = 0;
            }
            
            lastChar = c;
        }
    }
    
    if (containsSomething)
    {
        NSError *error = nil;
        static NSDataDetector *dataDetector = nil;
        if (dataDetector == nil)
            dataDetector = [NSDataDetector dataDetectorWithTypes:(int)(NSTextCheckingTypeLink | NSTextCheckingTypePhoneNumber) error:&error];
        
        NSMutableArray *results = [[NSMutableArray alloc] init];
        [dataDetector enumerateMatchesInString:_text options:0 range:NSMakeRange(0, _text.length) usingBlock:^(NSTextCheckingResult *match, __unused NSMatchingFlags flags, __unused BOOL *stop)
         {
             NSTextCheckingType type = [match resultType];
             if (type == NSTextCheckingTypeLink || type == NSTextCheckingTypePhoneNumber)
             {
                 [results addObject:match];
             }
         }];
        return results;
    }
    
    return nil;
}

+ (NSArray *) additionAttributeForUrls:(NSArray *)checkingResult{
    if (checkingResult == nil || checkingResult.count == 0)
    {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    for (NSTextCheckingResult *result  in checkingResult) {
        NSArray *fontAttributes = [[NSArray alloc] initWithObjects:(__bridge id)[UIColor whiteColor].CGColor, (NSString *)kCTForegroundColorAttributeName, @(kCTUnderlineStyleNone|kCTUnderlineStyleSingle), (NSString *)NSUnderlineStyleAttributeName,(__bridge id)[UIColor redColor].CGColor, NSUnderlineColorAttributeName, nil];
        
        NSRange range = NSMakeRange(result.range.location, result.range.length);
        [array addObjectsFromArray:[NSArray arrayWithObjects:[[NSValue alloc] initWithBytes:&range objCType:@encode(NSRange)],fontAttributes, nil]];
    }
    return array;
}

+ (NSInteger)yearOfTimeInterval:(NSTimeInterval)time {
    time_t t = time;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_year + 1900;
}

+ (NSString *)stringWithLocalizedNumberCharacters:(NSString *)string {
    NSString *resultString = string;
    
    if (IsArabic()) {
        static NSString *arabicNumbers = @"٠١٢٣٤٥٦٧٨٩";
        NSMutableString *mutableString = [[NSMutableString alloc] init];
        for (int i = 0; i < (int)string.length; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (c >= '0' && c <= '9')
                [mutableString replaceCharactersInRange:NSMakeRange(mutableString.length, 0) withString:[arabicNumbers substringWithRange:NSMakeRange(c - '0', 1)]];
            else
                [mutableString replaceCharactersInRange:NSMakeRange(mutableString.length, 0) withString:[string substringWithRange:NSMakeRange(i, 1)]];
        }
        resultString = mutableString;
    }
    
    return resultString;
}


+ (NSString *)stringWithLocalizedNumber:(NSInteger)number {
    return [Utils stringWithLocalizedNumberCharacters:[[NSString alloc] initWithFormat:@"%ld", (long)number]];
}

+ (NSString *)stringForDialogTime:(int)time {
    time_t t = time;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    
    return [[NSString alloc] initWithFormat:@"%@ %@", monthFull(timeinfo.tm_mon), [Utils stringWithLocalizedNumber:timeinfo.tm_mday]];
}

+ (NSInteger)monthFromDate:(NSDate *)date {
    time_t t = date.timeIntervalSince1970;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_mon + 1;
}

+ (NSInteger)dayFromDate:(NSDate *)date  {
    time_t t = date.timeIntervalSince1970;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_mday;
}

+ (NSInteger)yearFromDate:(NSDate *)date  {
    time_t t = date.timeIntervalSince1970;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_year + 1900;
}

+ (NSInteger)hourFromDate:(NSDate *)date {
    time_t t = date.timeIntervalSince1970;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_hour;
}

+ (NSInteger)minFromDate:(NSDate *)date {
    time_t t = date.timeIntervalSince1970;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    return timeinfo.tm_min;
}

+ (NSString *)stringForDialogTime:(int)time
                       isShowFull:(bool) isShowFull {
    time_t t = time * 24 * 60 * 60;
    struct tm timeinfo;
    gmtime_r(&t, &timeinfo);
    int month = timeinfo.tm_mon + 1;
    int year = timeinfo.tm_year + 1900;
    
    if (isShowFull){
        return [[NSString alloc] initWithFormat:@"%d/%d/%@ %@", year, month, [Utils stringWithLocalizedNumber:timeinfo.tm_mday], wdayFull(timeinfo.tm_wday)];
    } else {
        return [[NSString alloc] initWithFormat:@"%d/%@ %@",month, [Utils stringWithLocalizedNumber:timeinfo.tm_mday],wdayFull(timeinfo.tm_wday)];
    }
    
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (BOOL)is24hTimeFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:date];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    return (amRange.location == NSNotFound && pmRange.location == NSNotFound);
}

+ (BOOL)isAMTime:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:date];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    return (amRange.location != NSNotFound);
}

+ (NSString *)tempPath {
  NSString *str=  [NSTemporaryDirectory() stringByAppendingPathComponent:@"app"];
  BOOL isDir;
  if (![[NSFileManager defaultManager] fileExistsAtPath:str isDirectory:&isDir]) {
    [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
  }
  return str;
}

+ (NSData *)key {
    NSString *str = @"A6B6F6A5DD9761AD2FA38B6733D2D1QEAFA9BA52014BACC8BC198A2E1B6034B2";
    return [str dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)documentPath {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  return documentsDirectory;
}

+ (NSString *)recordedPath {
  NSString *str=  [[Utils documentPath] stringByAppendingPathComponent:@"recorded"];
  BOOL isDir;
  if (![[NSFileManager defaultManager] fileExistsAtPath:str isDirectory:&isDir]) {
    [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
  }
  return str;
}

+ (void)moveItem:(NSString *)url toURL:(NSString *)toURL {
    [[NSFileManager defaultManager] moveItemAtPath:url toPath:toURL error:nil];
}
@end
