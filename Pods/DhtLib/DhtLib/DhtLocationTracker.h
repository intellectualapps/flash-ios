//
//  DhtLocationTracker.h
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/13/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "DhtLocationModel.h"
#import <UIKit/UIKit.h>

typedef void (^LocationCompletionHandler)(CLLocation *location, NSError *error);
@interface DhtLocationTracker : NSObject<CLLocationManagerDelegate>
@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;

@property (strong,nonatomic) DhtLocationModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
@property (nonatomic) LocationCompletionHandler completionHandler;
+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (CLLocationCoordinate2D)bestCurrentLocationCoordinate;
- (CLLocation *)bestLocation;
- (void)clear;
@end
