//
//  DhtLocationTracker.m
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/13/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtLocationTracker.h"
#import "Utils.h"
#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface DhtLocationTracker()
@property (nonatomic, assign) BOOL isTracking;
@end
@implementation DhtLocationTracker
+ (CLLocationManager *)sharedLocationManager {
    static CLLocationManager *_locationManager;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
            _locationManager.allowsBackgroundLocationUpdates = YES;
        }
        _locationManager.pausesLocationUpdatesAutomatically = NO;
    });
    return _locationManager;
}

- (id)init {
    self  = [super init];
    if (self) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [DhtLocationModel shareModel];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
        [self restartLocationUpdates];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)applicationEnterBackground {
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;

    if (self.isTracking) {
        [locationManager requestAlwaysAuthorization];
        [locationManager startUpdatingLocation];
    }

    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [DhtBackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates {
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    [locationManager startUpdatingHeading];
}


- (void)startLocationTracking {
    self.isTracking = true;
    NSLog(@"startLocationTracking");
    if ([CLLocationManager locationServicesEnabled] == NO) {
        NSLog(@"locationServicesEnabled false");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    } else {
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];

        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
            NSLog(@"authorizationStatus failed");
        } else {
            NSLog(@"authorizationStatus authorized");
            [self restartLocationUpdates];
        }
    }
}


- (void)stopLocationTracking {
    self.isTracking = false;
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    for (int i = 0; i < locations.count; i++) {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
        if (locationAge > 30.0) {
            continue;
        }

        //Select only valid location and also location with good accuracy
        if(newLocation != nil &&
           theAccuracy > 0 &&
           theAccuracy < 2000 &&
           (!(theLocation.latitude == 0.0 &&
              theLocation.longitude == 0.0))) {
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            //only hold max 50 object
            if (self.shareModel.myLocationArray.count > 50) {
                for (NSInteger i = self.shareModel.myLocationArray.count - 1; i >= 0; i--) {
                    if (i > 50) {
                        [self.shareModel.myLocationArray removeObjectAtIndex:i];
                    }
                }
            }
            [self.shareModel.myLocationArray addObject: [newLocation copy]];
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationDidUpdateLocation object: newLocation];
        }
    }

    if (self.completionHandler) {
        self.completionHandler([self bestLocation], nil);
        self.completionHandler = nil;
    }
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }

    self.shareModel.bgTask = [DhtBackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];

    //Restart the locationMaanger after 1 minute
    self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self
                                                           selector:@selector(restartLocationUpdates)
                                                           userInfo:nil
                                                            repeats:NO];

    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    if (self.shareModel.delay10Seconds) {
        [self.shareModel.delay10Seconds invalidate];
        self.shareModel.delay10Seconds = nil;
    }

    self.shareModel.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                                    selector:@selector(stopLocationDelayBy10Seconds)
                                                                    userInfo:nil
                                                                     repeats:NO];

}


//Stop the locationManager
-(void)stopLocationDelayBy10Seconds {
    //    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    //    [locationManager stopUpdatingLocation];
}

- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error {
    if (self.completionHandler) {
        self.completionHandler([self bestLocation], error);
        self.completionHandler = nil;
    }
}

- (CLLocation *)bestLocation {
    CLLocation * myBestLocation = nil;
    for(int i = 0; i < self.shareModel.myLocationArray.count; i++) {
        CLLocation* currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        if (i == 0) {
            myBestLocation = currentLocation;
        } else {
            if (currentLocation.horizontalAccuracy <= myBestLocation.horizontalAccuracy) {
                myBestLocation = currentLocation;
            }
        }
    }
    return myBestLocation;
}

- (CLLocationCoordinate2D)bestCurrentLocationCoordinate {
    CLLocation * myBestLocation = nil;
    for (int i = 0; i < self.shareModel.myLocationArray.count; i++) {
        CLLocation* currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        if (i == 0) {
            myBestLocation = currentLocation;
        } else {
            if(currentLocation.horizontalAccuracy <= myBestLocation.horizontalAccuracy) {
                myBestLocation = currentLocation;
            }
        }
    }
    if (self.shareModel.myLocationArray.count == 0) {
        self.myLocation = self.myLastLocation;
        self.myLocationAccuracy = self.myLastLocationAccuracy;
        
    } else {
        self.myLocation = myBestLocation.coordinate;
        self.myLocationAccuracy = myBestLocation.horizontalAccuracy;
    }
    return self.myLocation;
}

- (void)clear {
    [self.shareModel.myLocationArray removeAllObjects];
    self.shareModel.myLocationArray = nil;
    self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
}
@end
