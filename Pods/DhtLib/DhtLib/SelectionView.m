//
//  SelectionView.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/15/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "SelectionView.h"

@implementation SelectionView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _shadowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CheckmarkShadow"]];
        _checkMarView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [self addSubview:_shadowView];
        [self addSubview:_checkMarView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize sadowSize = _shadowView.image.size;
    _shadowView.frame = CGRectMake(
                                   self.frame.size.width - sadowSize.width - 10,
                                   self.frame.size.height - sadowSize.height - 10,
                                   sadowSize.width,
                                   sadowSize.height
                                   );
    _checkMarView.center = _shadowView.center;
}
@end
