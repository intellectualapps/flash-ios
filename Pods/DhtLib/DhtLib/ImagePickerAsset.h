//
//  ImagePickerAsset.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ImagePickerAsset : NSObject
@property (nonatomic, strong) ALAsset *asset;
@property (nonatomic, strong) NSString *assetUrl;
@property (nonatomic, strong) NSString *desc;

@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic) bool isLoading;
@property (nonatomic) bool isLoaded;
@property (nonatomic) bool isChecked;
@property (nonatomic) bool cameraAsset;

- (id)initWithAsset:(ALAsset *)asset;
- (id)init;
- (void)load;
- (void)unload;
- (UIImage *)forceLoadedThumbnailImage;
@end
