//
//  DhtTargetTimer.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/5/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DhtTargetTimer : NSObject
@property (nonatomic, weak) id target;
@property (nonatomic) SEL action;

+ (NSTimer *)scheduledMainThreadTimerWithTarget:(id)target action:(SEL)action interval:(NSTimeInterval)interval repeat:(bool)repeat;
+ (NSTimer *)scheduledMainThreadTimerWithTarget:(id)target action:(SEL)action interval:(NSTimeInterval)interval repeat:(bool)repeat runLoopModes:(NSString *)runLoopModes;
@end
