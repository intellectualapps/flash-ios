//
//  Zakatify-Bridging-Header.h
//  Zakatify
//
//  Created by tran vuong minh on 6/3/17.
//  Copyright © 2017 Dht. All rights reserved.
//

#ifndef Zakatify_Bridging_Header_h
#define Zakatify_Bridging_Header_h
@import FacebookCore;
@import RxSwift;
@import RxCocoa;
#import "JSQSystemSoundPlayer.h"
#import "JSQMessagesViewController.h"
#import "JSQMessagesBubbleImageFactory.h"
#import "JSQMessage.h"
#import "JSQMessageAvatarImageDataSource.h"
#endif /* Zakatify_Bridging_Header_h */
