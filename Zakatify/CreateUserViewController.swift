//
//  CreateUserViewController.swift
//
//
//  Created by Thang Truong on 5/28/17.
//
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift

class CreateUserViewController: UIViewController {
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    
    var user: UserInfo?
    var presenter: UpdateUserInfoPresenter?
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.text = user?.email
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let user = UserManager.shared.currentUser {
            presenter = UpdateUserInfoPresenter(view: self, model: user)
            presenter?.getSocialProfile()
        }
        
        usernameTextField.rx.text.orEmpty.asObservable().subscribe { [unowned self] (event) in
            switch event {
            case .next(let text):
                self.presenter?.username = text
            case .error(_):
                break
            case .completed:
                break
            }
            }.addDisposableTo(disposeBag)
        
        emailTextField.rx.text.orEmpty.asObservable().subscribe { [unowned self] (event) in
            switch event {
            case .next(let text):
                self.presenter?.userEmail = text
            case .error(_):
                break
            case .completed:
                break
            }
        }.addDisposableTo(disposeBag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO : assign a.Thang
    
    fileprivate func gotoIntroView() {
        performSegue(withIdentifier: "ShowIntroVC", sender: nil)
    }
    
    @IBAction func onClickCreate(_ sender: Any) {
        presenter?.saveChange()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CreateUserViewController: UpdateUserInfoView {
    func saveSuccess() {
        gotoIntroView()
    }
    
    func updateSocicalProfile() {
        guard let presenter = presenter else {
            return
        }
        if let url = URL(string: presenter.socialPhotoUrl) {
            iv_avatar.sd_setImage(with: url,
                                placeholderImage: UIImage(named: "noAvatar"))
        }
        lb_name.text = presenter.usersocialFullName
        lb_username.text = presenter.username
        emailTextField.text = presenter.socialEmail
        presenter.userEmail = presenter.socialEmail
    }
}


