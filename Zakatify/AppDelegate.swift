//
//  AppDelegate.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Reachability
import FBSDKCoreKit
import TwitterKit
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var networkReachability: Reachability?
    var window: UIWindow?
    class func shareInstance() -> AppDelegate {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            return delegate
        }
        return AppDelegate()
    }

    func setupNetworkListing() {
        networkReachability = Reachability.forInternetConnection()
        networkReachability?.reachableBlock = { (reachbility) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NotificationName.NetworkStateChange.name(), object: nil)
            }
        }
        networkReachability?.unreachableBlock = { (reachbility) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NotificationName.NetworkStateChange.name(), object: nil)
            }
        }
        networkReachability?.startNotifier()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //enable keyboard manager
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        // Override point for customization after application launch.
        let center = UNUserNotificationCenter.current()
        UNUserNotificationCenter.current().delegate = self
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
        //FBSDK config
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        // Twitter
        Twitter.sharedInstance().start(withConsumerKey: "Ym5f8VAsYTToepl9hMx3eZwfh", consumerSecret: "WbjsPeP3QzVdKWJ2bmKTIiq5ARqxYgVcbWZCzSTX0WutimRD5h")
        //
        setupPaypalSDK()
        //
        FirebaseApp.configure()
        //

        if let _ = UserManager.shared.authenToken {
            if let user = UserManager.shared.currentUser {
                if user.username.isEmpty {
                    let vc = UIStoryboard.main().instantiateViewController(withIdentifier: "createUser")
                    window?.rootViewController = vc
                } else if user.firstName.isEmpty {
                    let vc = UIStoryboard.completeProfile().instantiateInitialViewController()
                    window?.rootViewController = vc
                } else {
                    gotoHomeScreen()
                }
            }
        } 
        return true
    }

    func gotoHomeScreen() {
        let vc = UIStoryboard.home().instantiateInitialViewController()
        window?.rootViewController = vc
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if Twitter.sharedInstance().application(app, open: url, options: options) {
            return true
        }
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        AppEventsLogger.activate()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        if let user = UserManager.shared.currentUser {
            let service = UserServicesCenter()
            service.registerDeviceToken(username: user.username, deviceToken: token, complete: { (result) in
                switch result {
                case .success(_):
                    print("Register device token success")
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
    }

}


import Cloudinary
extension AppDelegate {
    var cloudinary: CLDCloudinary {
        let config = CLDConfiguration(cloudinaryUrl: "cloudinary://831539118467619:9ITVH8THpDY9WRSfbswt5Om9Hvg@pentor")!
        let cloudinary = CLDCloudinary(configuration: config)
        return cloudinary
    }
    
    func gotoHomeViewController() {
        if let _ = UserManager.shared.currentUser {
            let vc = UIStoryboard.home().instantiateInitialViewController()
            var lastRootController = self.window?.rootViewController
            if let currentView = lastRootController?.view, let view = vc?.view {
                DispatchQueue.main.async {
                    UIView.transition(from: currentView, to: view, duration: 0.3, options: UIViewAnimationOptions.curveLinear, completion: { (finished) in
                        self.window?.rootViewController = vc
                        lastRootController?.view?.removeAllSubviews()
                        lastRootController?.removeFromParentRecursive()
                        lastRootController = nil
                    })
                }
            }
        }
    }
    
    func gotoLoginViewController() {
        let vc = UIStoryboard.main().instantiateInitialViewController()
        if let currentView = self.window?.rootViewController?.view, let view = vc?.view {
            DispatchQueue.main.async {
                UIView.transition(from: currentView, to: view, duration: 0.3, options: UIViewAnimationOptions.curveLinear, completion: { (finished) in
                    self.window?.rootViewController = vc
                })
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.identifier == "Local.Alert" {
            completionHandler(UNNotificationPresentationOptions.alert)
            return
        }
    }
}

extension AppDelegate {
    func setupPaypalSDK() {
    }
}

