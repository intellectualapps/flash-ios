//
//  SearchResultViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import SVPullToRefresh

class SearchResultViewController: UITableViewController, TableView, ListZakatifiersView {
    
    
    var searchString: Variable<String> = Variable(String())
    var dispose = DisposeBag()
    enum ScopeType: String {
        case charity = "CHARITIES"
        case zakatifier = "ZAKATIFIERS"
    }
    var viewModel = SearchResultViewModel()
    var type: ScopeType = .charity
    var charityPresenter: CharityListViewPresenter?
    var zakatifierPresenter: ListZakatifiersPresenter?
    var refPresenter: TableViewPresenter? {
        return charityPresenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupObservable()
        
        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)
        tableView.register(UINib(nibName: "ZakatifierTableViewCell", bundle: nil), forCellReuseIdentifier: ZakatifierTableViewCell.identifier)

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UITableViewHeaderFooterView()
        charityPresenter = CharityListPresenter(view: self)
        zakatifierPresenter = ListZakatifiersPresenter(view: self)

        if #available(iOS 11.0, *) {
            //            self.edgesForExtendedLayout = .all
        } else {
            self.edgesForExtendedLayout = .top
        }
        
        charityPresenter?.charities
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (data) in
                if let strongself = self , data.count > 0 {
                    strongself.tableView.reloadData()
                    strongself.tableView.infiniteScrollingView.stopAnimating()
                }
            }).disposed(by: dispose)
        
        tableView.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else {
                return
            }
            switch self.type {
            case .charity:
                self.charityPresenter?.loadmoreSearch(keyword: self.searchString.value)
            case .zakatifier:
                self.getSearchZakatifier(isLoadMore: true, page: self.viewModel.zakatifierPage)
            }
        })
    }
    
    private func setupObservable() {
        searchString.asObservable()
            .throttle(1.5, scheduler: MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: { [weak self] text in
                guard let `self` = self else { return }
                switch self.type {
                case .charity:
                    self.charityPresenter?.refresh(search: text)
                case .zakatifier:
                    self.viewModel.zakatifierPage = 1
                    self.getSearchZakatifier(isLoadMore: false, page: self.viewModel.zakatifierPage)
                }
            })
            .disposed(by: dispose)
    }
    
    func getSearchZakatifier(isLoadMore: Bool, page: Int) {
        viewModel.zakatifierSearch(username: UserManager.shared.currentUser?.username ?? "", keyword: searchString.value, page: page, size: 20) { [weak self] (response, error) in
            guard let `self` = self,
                let response = response as? [UserPublicProfile] else { return }
            if !isLoadMore {
                self.viewModel.resultSearchZakatifiers = response
            } else {
                for userPublicProfile in response {
                    if self.viewModel.validateZakatifierModel(zakatifierModel: userPublicProfile) {
                        self.viewModel.resultSearchZakatifiers.append(userPublicProfile)
                    }
                }
                self.viewModel.zakatifierPage = self.viewModel.resultSearchZakatifiers.count / 20 + 1
                self.tableView.infiniteScrollingView.stopAnimating()
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        if #available(iOS 11.0, *) {
//            tableView.contentInset = UIEdgeInsets(top: 94, left: 0, bottom: 0, right: 0)
//        } else {
//            tableView.contentInset = UIEdgeInsets(top: 94+64, left: 0, bottom: 0, right: 0)
//        }
    }

    func refreshControlChange() {
        tableView.reloadData()
        refreshControl?.endRefreshing()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        switch self.type {
        case .charity:
            return charityPresenter?.numberOfSections() ?? 0
        case .zakatifier:
            return zakatifierPresenter?.numberOfSections() ?? 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.type {
        case .charity:
            return charityPresenter?.numberOfRowsInSection(section: section) ?? 0
        case .zakatifier:
            return viewModel.resultSearchZakatifiers.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == .zakatifier {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ZakatifierTableViewCell.identifier, for: indexPath) as? ZakatifierTableViewCell else {
                return UITableViewCell()
            }
            
            let zakatifier = viewModel.resultSearchZakatifiers[indexPath.row]
            cell.zakatifier = zakatifier
            
            cell.clickAddBlock = { [weak self] zakatifier in
                guard let zakatifier = zakatifier else { return }
                self?.zakatifierPresenter?.addZakatifier(zakatifier)
            }
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CharityDetailTableViewCell.identifier, for: indexPath) as? CharityDetailTableViewCell else {
                return UITableViewCell()
            }
            if let model = charityPresenter?.charity(index: indexPath.row) {
                let cellPresenter = CharityDetailPresenter(view: cell, model: model)
                cell.presenter = cellPresenter
                cell.clickAddBlock = { [unowned self] in
                    let params: AppEvent.ParametersDictionary = [
                        AppEventParameterName.custom("screen_name"): cellPresenter.charityName
                    ]
                    UIViewController.logEvent(eventName: .follow, parameters: params)
                    self.charityPresenter?.add(charity: model)
                }
            } else {
                cell.presenter = nil
                cell.clickAddBlock = nil
            }
            return cell
        }
    }
    
//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
//        if indexPath.row == lastElement - 1 {
//            refPresenter?.loadmore()
//        }
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if type == .zakatifier {
            let model = viewModel.resultSearchZakatifiers[indexPath.row]
            guard let vc = UIStoryboard.zakatifier().instantiateViewController(withIdentifier: "ProfileDetailsViewController") as? ProfileDetailsViewController else {
                return
            }
            vc.presenter = ZakatifierDetailsPresenter(view: vc, model: model)
            let navigation = NavigationController(rootViewController: vc)
            UIViewController.topMostViewController()?.present(navigation, animated: true, completion: nil)
            
        } else {
            if let model = charityPresenter?.charity(index: indexPath.row) {
                guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
                    return
                }
                let params: AppEvent.ParametersDictionary = [
                    AppEventParameterName.custom("screen_name"): model.name
                ]
                UIViewController.logEvent(eventName: .openProfile, parameters: params)
                vc.charity = model
                let navigation = NavigationController(rootViewController: vc)
//                self.present(navigation, animated: true, completion: nil)
                UIViewController.topMostViewController()?.present(navigation, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - UISearchBarDelegate
extension SearchResultViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        if let scopeTitle = searchBar.scopeButtonTitles?[selectedScope] {
            if let scope = ScopeType(rawValue: scopeTitle) {
                if scope == ScopeType.zakatifier {
                    UIViewController.logEvent(eventName: .zakatifiersTab)
                }
                self.type = scope
            }
        }
        refreshControlChange()
    }
}

// MARK: - UISearchResultsUpdating
extension SearchResultViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let keyword = searchController.searchBar.text {
            searchString.value = keyword
            if let scopeTitle = searchController.searchBar.scopeButtonTitles?[searchController.searchBar.selectedScopeButtonIndex] {
                if let scope = ScopeType(rawValue: scopeTitle) {
                    self.type = scope
                }
            }
            
        }
    }
}
