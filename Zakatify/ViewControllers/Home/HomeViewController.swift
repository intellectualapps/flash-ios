//
//  HomeViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import SVPullToRefresh
import RxSwift

class HomeViewController: UITableViewController, TableView {
    var presenter: CharityListViewPresenter!
    var didSetup = false
    var disposed = DisposeBag()
    var refPresenter: TableViewPresenter? {
        guard let presenter = self.presenter else {
            return nil
        }
        return presenter
    }
    
    lazy var searchNavigationController: UINavigationController = {
        let navigation = UIStoryboard.home().instantiateViewController(withIdentifier: "searchNavition") as! UINavigationController
        return navigation
    }()
    
    lazy var searchController: UISearchController = { [unowned self] in
        let searchResultsController = UIStoryboard.home().instantiateViewController(withIdentifier: "SearchResultViewController") as! SearchResultViewController
        searchResultsController
            .searchString
            .asObservable()
            .subscribe(onNext: { [weak self] str in
                guard let `self` = self else { return }
                if str == "" {
                    if self is EditPortfolioViewController {
                        self.title = "Edit portfolio"
                    } else {
                        self.title = "Welcome to Zakatify"
                    }
                } else {
                    self.title = "Search Zakatify"
                }
            }).disposed(by: self.disposed)
        let searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = searchResultsController
        searchController.dimsBackgroundDuringPresentation = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.delegate = searchResultsController
        let searchBar = searchController.searchBar
        searchBar.placeholder = "Search charities & zakatifiers"
        searchBar.tintColor = UIColor.navigationBartinColor
        searchBar.setSearchFieldBackgroundImage(#imageLiteral(resourceName: "SearchBar"), for: UIControlState.normal)

        searchBar.scopeButtonTitles = [SearchResultViewController.ScopeType.charity.rawValue,
                                       SearchResultViewController.ScopeType.zakatifier.rawValue]
        searchBar.scopeBarBackgroundImage = UIImage(color: UIColor.white)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope"), for: UIControlState.selected)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope"), for: UIControlState.highlighted)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope-unselect"), for: UIControlState.normal)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.darkGray], for: UIControlState.highlighted)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.darkGray], for: UIControlState.selected)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.lightGray], for: UIControlState.normal)
        
        searchBar.setScopeBarButtonDividerImage(UIImage(color: UIColor.white, size: CGSize(width: 5, height: 44)), forLeftSegmentState: .normal, rightSegmentState: .highlighted)
        searchBar.setScopeBarButtonDividerImage(UIImage(color: UIColor.white, size: CGSize(width: 5, height: 44)), forLeftSegmentState: .highlighted, rightSegmentState: .normal)
        
        if let scope = searchBar.traverseSubviewForViewOfKind(kind: UISegmentedControl.self) {
            scope.frame.origin.y = 0
            scope.frame.size.height = 44
        }
        return searchController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setup() {
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if #available(iOS 11.0, *) {
//            searchController.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 54)
//            self.view?.addSubview(searchController.searchBar)
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
            navigationController?.navigationBar.prefersLargeTitles = false
            searchController.searchBar.tintColor = UIColor.white
            //            let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
            //            textFieldInsideSearchBar?.textColor = UIColor.white

            //            self.tableView.tableHeaderView = searchController.searchBar
            //            searchController.hidesNavigationBarDuringPresentation = false
        } else {
            //searchController.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = searchController.searchBar
        }
        self.tableView.sectionHeaderHeight = 0.1
        self.tableView.contentInset.top = 55
        self.tableView.tableFooterView = UITableViewHeaderFooterView()
        self.definesPresentationContext = true

        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }

        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        presenter = CharityListPresenter(view: self)
        presenter.charities
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (data) in
                if let strongself = self , data.count > 0 {
                    strongself.tableView.reloadData()
                    strongself.tableView.infiniteScrollingView.stopAnimating()
                }
            }).disposed(by: disposed)

        tableView.addInfiniteScrolling(actionHandler: {[weak self] in
            if let strongself = self {
                if let refreshControl = strongself.refreshControl, refreshControl.isRefreshing {
                    strongself.tableView.infiniteScrollingView.stopAnimating()
                    return
                }
                strongself.presenter?.loadmore()
            }
        })
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didSetup {
            didSetup = true
            setup()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter.refresh()
    }

    var isSet = false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.frame.origin.y = 12
            if let subviews = textFieldInsideSearchBar?.superview?.subviews {
                if subviews.count >= 4 {
                    subviews[3].frame.origin.y = 12
                }
            }
            if let scope = textFieldInsideSearchBar?.superview?.traverseSubviewForViewOfKind(kind: UISegmentedControl.self) {
                scope.frame.origin.y = 0
                scope.frame.size.height = 44
            }
            guard isSet == false else {
                return
            }
            isSet = true
            textFieldInsideSearchBar?.superview?.backgroundColor = UIColor.lightGray
            textFieldInsideSearchBar?.tintColor = UIColor.navigationBartinColor
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func refreshControlChange() {
        presenter?.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logout(_ sender: Any) {
        UserManager.shared.logout()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CharityDetailTableViewCell.identifier, for: indexPath) as? CharityDetailTableViewCell else {
            return UITableViewCell()
        }
        if let model = presenter.charity(index: indexPath.row) {
            let cellPresenter = CharityDetailPresenter(view: cell, model: model)
            cell.presenter = cellPresenter
            cell.clickAddBlock = { [unowned self] in
                let params: AppEvent.ParametersDictionary = [
                    AppEventParameterName.custom("screen_name"): cellPresenter.charityName
                ]
                UIViewController.logEvent(eventName: .follow, parameters: params)
                self.presenter?.add(charity: model)
            }
            
            cell.clickRemoveBlock = { [unowned self] in
                self.presenter?.remove(charity: model)
            }
        } else {
            cell.presenter = nil
            cell.clickAddBlock = nil
        }
        return cell
    }

//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
//        if indexPath.row == lastElement - 1 {
//            presenter?.loadmore()
//        }
//    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
            return
        }
        if let model = presenter.charity(index: indexPath.row) {
            let params: AppEvent.ParametersDictionary = [
                AppEventParameterName.custom("screen_name"): model.name
            ]
            UIViewController.logEvent(eventName: .openProfile, parameters: params)
            vc.charity = model
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.tableView.contentInset.top = 0
    }

}


