//
//  SideMenuViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController: UITableViewController, SideMenuView {
    //
    @IBOutlet weak var iv_RateChart: CircleView!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_rateValue: UILabel!
    //
    @IBOutlet weak var lb_fullName: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    //
    @IBOutlet weak var lb_moneyDescription: UILabel!
    //
    @IBOutlet weak var lb_rankDescription: UILabel!

    @IBOutlet weak var lb_messages_count: BabgeLabel!
    
    var presenter: SideMenuViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
//        SideMenuManager.menuPushStyle = .preserveAndHideBackButton
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuBlurEffectStyle = nil
        SideMenuManager.menuWidth = UIScreen.main.bounds.size.width - 50
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuEnableSwipeGestures = true

        if let _ = UserManager.shared.currentUser {
            presenter = SideMenuPresenter(view: self)
            presenter?.refresh()
        }
        fillData()
    }
    
    func fillData() {
        //
        guard let presenter = presenter else {
            return
        }
        if let url = presenter.avartarURL {
            iv_avatar.sd_setImage(with: url,
                                  placeholderImage: UIImage(named: "noAvatar"))
        }
        iv_RateChart.value = CGFloat(presenter.percentCompleted)
        lb_rateValue.text = "\(Float(presenter.percentCompleted))%"
        //
        lb_fullName.attributedText = presenter.nameAttributedString
        lb_username.text = "@" + presenter.username
        //
        let moneyDescription = NSMutableAttributedString()
        let currentMoney = NSAttributedString(string: "$\(Int(presenter.totalDonation))",
                                              attributes: [NSForegroundColorAttributeName:UIColor.green,
                                                           NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)])
        moneyDescription.append(currentMoney)
        
        let totalMoney = NSAttributedString(string: " out of $\(Int(presenter.goal))",
                                              attributes: [NSForegroundColorAttributeName:UIColor.green,
                                                           NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        moneyDescription.append(totalMoney)
        
        lb_moneyDescription.attributedText = moneyDescription
        //
        lb_rankDescription.attributedText = presenter.rankDescriptionAttributedString
        
        lb_messages_count?.text = "\(presenter.message)"
        lb_messages_count?.isHidden = presenter.message == 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeMenu(_ sender: Any) {
        SideMenuManager.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func logout(_ sender: Any) {
        SideMenuManager.menuLeftNavigationController?.dismiss(animated: false, completion: {
            UserManager.shared.logout()
        })
    }

    @IBAction func clickTopZakatifiers(_ sender: Any) {
//        if let vc = UIStoryboard.zakatifier().instantiateViewController(withIdentifier: "ZakatifiersTableViewController") as? ZakatifiersTableViewController {
//            SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
//        }
        if let vc = UIStoryboard.zakatifier().instantiateViewController(withIdentifier: "ZakatifyTopController") as? ZakatifyTopController {
            SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func clickInvite(_ sender: Any) {
        /**
        if let vc = UIStoryboard.invite().instantiateViewController(withIdentifier: "InviteTableViewController") as? InviteTableViewController {
            SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
        }
         */
        
        // Open web view
        guard let username = UserManager.shared.currentUser?.username else {
            return
        }
        guard let url = URL(string: "https://zakatify.com/app/inv/\(username)?key=eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzIwMTk0ODcsImlzcyI6InBodXMiLCJleHAiOjE1MzIxMDU4ODd9.-7H1PnnyPbSo1xNYQ7IAOci78PoZ0XONPIaxWAo22-E") else {
            return
        }
        openWebview(url: url, title: "Invite Friends")
    }
    
    @IBAction func clickTerms(_ sender: Any) {
        openWebview2(type: .terms, title: "Terms")
    }
    @IBAction func clickAbout(_ sender: Any) {
//        if let vc = UIStoryboard.about().instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController {
//            SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
//        }
        openWebview2(type: .about, title: "About")
    }
    
    @IBAction func clickHelp(_ sender: Any) {
        openWebview2(type: .qa, title: "Q&A")
    }
    
    @IBAction func clickSecurity(_ sender: Any) {
    }
    
    @IBAction func clickPrivacy(_ sender: Any) {
        openWebview2(type: .privacy, title: "Privacy")
    }
    @IBAction func clickContactUs(_ sender: Any) {
        guard let username = UserManager.shared.currentUser?.username else { return }
        guard let url = URL(string: "https://zakatify.com/app/con/\(username)?key=eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzIwMTk0ODcsImlzcyI6InBodXMiLCJleHAiOjE1MzIxMDU4ODd9.-7H1PnnyPbSo1xNYQ7IAOci78PoZ0XONPIaxWAo22-E") else { return }
        openWebview(url: url, title: "Contact Us")
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                SideMenuManager.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
                break
            case 1:
                if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "EditProfileContainerViewController") as? EditProfileContainerViewController {
                    if let percent = presenter?.sideMenuModel.donationProgress.percentCompleted {
                        vc.percentCompleted = percent
                    }
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
                break
            case 2:
                if let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "EditPortfolioViewController") as? EditPortfolioViewController {
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
                break
            case 3:
                if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "PaymentsViewController") as? PaymentsViewController {
                    vc.mode = .edit
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
                break
            case 4:
                if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "PreferencesViewController") as? PreferencesViewController {
                    vc.mode = .edit
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
            case 6:
                if let vc = UIStoryboard.message().instantiateViewController(withIdentifier: "MessagesTableViewController") as? MessagesTableViewController {
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
            case 5:
                if let username = UserManager.shared.currentUser?.username {
                    if let url = URL(string: "https://zakatify.com/app/don/\(username)?key=eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzIwMTk0ODcsImlzcyI6InBodXMiLCJleHAiOjE1MzIxMDU4ODd9.-7H1PnnyPbSo1xNYQ7IAOci78PoZ0XONPIaxWAo22-E") {
                        openWebview(url: url, title: "Donation History")
                    }
                }
            default:
                break
            }
        } else if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                if let vc = UIStoryboard.completeProfile().instantiateViewController(withIdentifier: "EditProfileContainerViewController") as? EditProfileContainerViewController {
                    if let percent = presenter?.sideMenuModel.donationProgress.percentCompleted {
                        vc.percentCompleted = percent
                    }
                    SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
                }
                break
            default:
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func openWebview2(type: WebviewUrl, title: String) {
        if let vc = UIStoryboard.webview().instantiateViewController(withIdentifier: "WebviewController") as? WebviewController {
            vc.title = title
            switch type {
            case .contact:
                vc.url_redirect = type.rawValue + "?id=\(UserManager.shared.currentUser?.username ?? "")"
            default:
                vc.url_redirect = type.rawValue
            }
            SideMenuManager.menuLeftNavigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension SideMenuViewController {
    override func showLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    override func hideLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
