//
//  WebviewController.swift
//  Zakatify
//
//  Created by center12 on 6/6/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

enum WebviewUrl:String {
    case privacy = "https://www.zakatify.com/txt/privacy.html"
    case terms = "https://www.zakatify.com/txt/terms.html"
    case qa = "https://www.zakatify.com/txt/faq.html"
    case about = "https://www.zakatify.com/txt/about.html"
    case contact = "https://www.zakatify.com/contact"
    case charity = ""
}

class WebviewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var progressBarView: UIProgressView!
    
    var url_redirect = ""
    var theBool: Bool = false
    var time: Timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView?.delegate = self
        self.addBackButtonDefault()
        self.addSwipeRight()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        loadWebContent()
    }
    
    func loadWebContent() {
        if let aurl = URL(string: url_redirect) {
            let urlRequest = URLRequest(url: aurl)
            self.webView?.loadRequest(urlRequest)
        }
    }
    
    func timerCallback() {
        if self.theBool {
            if self.progressBarView.progress >= 1 {
                self.progressBarView.isHidden = true
                self.time.invalidate()
            } else {
                self.progressBarView.progress += 0.1
            }
        } else {
            self.progressBarView.progress += 0.05
            if self.progressBarView.progress >= 0.95 {
                self.progressBarView.progress = 0.95
            }
        }
    }
}

extension WebviewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.progressBarView.progress = 0.0
        self.theBool = false
        self.time = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(self.timerCallback), userInfo: nil, repeats: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.theBool = true
    }
}
