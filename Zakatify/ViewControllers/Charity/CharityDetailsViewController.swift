//
//  CharityDetailsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class CharityDetailsViewController: UIViewController {
    @IBOutlet weak var bt_review: Button!
    
    var charityDetailTableView :CharityDetailsTableViewController?
    var presenter: CharityDetailViewPresenter? {
        didSet {
            charityDetailTableView?.presenter = presenter
        }
    }
    var charity: CharityDetail?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
            self.addSwipeRight()
        }

        if let charity = self.charity {
            presenter = CharityDetailPresenter(view: self, model: charity)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.refresh()
    }

    @IBAction func review(_ sender: Any) {
        guard let charity = self.charity else { return }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
        let presenter = CharityDetailPresenter(view: vc, model: charity)
        vc.presenter = presenter
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CharityDetailsTableViewController" {
            if let vc = segue.destination as? CharityDetailsTableViewController {
                self.charityDetailTableView = vc
                vc.presenter = presenter
            }
        }
    }
}

extension CharityDetailsViewController: CharityDetailView {
    var refTableView: UITableView? {
        guard let presenter = presenter else {
            return nil
        }
        return self.charityDetailTableView?.tableView
    }

    var refPresenter: TableViewPresenter? {
        return nil
    }

    func loadingMore() {
        guard let tableView = refTableView else {
            return
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        indicator.center = CGPoint(x: tableView.frame.size.width/2.0, y: 15)
        indicator.startAnimating()
        view.addSubview(indicator)
        refTableView?.tableFooterView = view
    }

    private func addLoadMoreView() {
        guard let tableView = refTableView else {
            return
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        let bt_loadmore = UIButton(type: UIButtonType.custom)
        bt_loadmore.frame = CGRect(x: 15, y: 0, width: tableView.frame.size.width - 30, height: 30)
        bt_loadmore.addTarget(self, action: #selector(loadmoreReviews), for: UIControlEvents.touchUpInside)
        bt_loadmore.setTitle("More reviews", for: UIControlState.normal)
        bt_loadmore.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        bt_loadmore.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
        bt_loadmore.contentHorizontalAlignment = .right
        view.addSubview(bt_loadmore)
        tableView.tableFooterView = view
    }

    func loadedMore() {
        guard let tableView = refTableView else {
            return
        }
        tableView.reloadData()
    }

    func loadmoreReviews() {
        presenter?.loadmore()
    }
    func userDidAddToPorfolio() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func userDidDonate() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func userDidWriteReview() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func dataChanged() {
        guard let presenter = presenter else {
            return
        }
        let bt_title = presenter.reviews.value.count == 0 ? "Write the first review" : "Write a review"
        bt_review.setTitle(bt_title, for: UIControlState.normal)
        self.charityDetailTableView?.tableView.reloadData()
        if presenter.shouldLoadmore() == true {
            addLoadMoreView()
        } else {
            guard let tableView = refTableView else {
                return
            }
            tableView.tableFooterView = nil
        }
    }
    
    func addPortfolio() {
        presenter?.addToPortfolio()
    }
    
    func removePortfolio() {
        self.charityDetailTableView?.tableView.reloadData()
    }
}
