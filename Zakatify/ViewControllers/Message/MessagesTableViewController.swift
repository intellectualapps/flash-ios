//
//  MessagesTableViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class MessagesTableViewController: UITableViewController {

    var presenter: MessageListPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSwipeRight()
        self.title = "Messages"

        guard let user = UserManager.shared.currentUser else {
            return
        }
        self.addBackButtonDefault()
        //
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        tableView.register(UINib(nibName: "ZakatifierTableViewCell", bundle: nil), forCellReuseIdentifier: ZakatifierTableViewCell.identifier)

        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.presenter = MessageListPresenter(view: self, user: user)
        presenter?.refresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.refresh()
    }

    func refreshControlChange() {
        presenter?.refresh()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageChannelTableViewCell.reuseIdentifier,
                                                 for: indexPath) as! MessageChannelTableViewCell

        if let room = presenter?.roomAt(index: indexPath) {
            cell.room = room
            cell.delegate = self
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let room = presenter?.roomAt(index: indexPath) else {
            return
        }
        guard let user = room.user, let otherUser = room.otherParticipant,
            let vc = UIStoryboard.message().instantiateViewController(withIdentifier: "MessageViewController") as? MessageViewController else {
            return
        }
        let chatPresenter = MessagePresenter(view: vc, user: user, otherUser: otherUser)
        chatPresenter.roomID = room.id
        vc.presenter = chatPresenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MessagesTableViewController: MessageListPresenterView {
    var refPresenter: TableViewPresenter? {
        return presenter
    }
}

// MARK: - MessageChannelTableViewCellDelegate
extension MessagesTableViewController: MessageChannelTableViewCellDelegate {
    func messageChannelTableViewCellDidReceiveNewRoom() {
        presenter?.list.sort { (room1, room2) -> Bool in
            let date1 = room1.lastDate?.timeIntervalSince1970 ?? 0
            let date2 = room2.lastDate?.timeIntervalSince1970 ?? 0
            return date1 > date2
        }
        tableView.reloadData()
    }
}
