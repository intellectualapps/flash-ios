//
//  MessageChannelTableViewCell.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

protocol MessageChannelTableViewCellDelegate: class {
    func messageChannelTableViewCellDidReceiveNewRoom()
}

class MessageChannelTableViewCell: UITableViewCell {
    var dispose = DisposeBag()

    @IBOutlet weak var badgeLbl: BabgeLabel!
    static let reuseIdentifier: String = "MessageChannelTableViewCell"
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_message: UILabel!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    weak var delegate: MessageChannelTableViewCellDelegate?

    var room: Room? {
        didSet {
            clearData()
            guard let room = room else {
                return
            }
            
            if let url = room.otherParticipant?.avartarURL {
                iv_avatar.sd_setImage(with: url,
                                      placeholderImage: UIImage(named: "noAvatar"))
            }
            lb_name.text = room.otherParticipant?.fullname
            guard let user = room.user, let otheruser = room.otherParticipant else {
                return
            }
            if let lastDate = room.lastDate {
                lb_date.text = lastDate.timeAgoDisplay()
                lb_message.text = room.lastMessage
            }
            let presenter = MessagePresenter(view: self, user: user, otherUser: otheruser, queryLimit: 1)
            self.presenter = presenter
            room.unreadObserble.asObservable()
                .subscribe(onNext: {[weak self] (unread) in
                    DispatchQueue.main.async {
                        self?.badgeLbl?.text = String(unread)
                        self?.badgeLbl?.setNeedsLayout()
                    }
                }).disposed(by: dispose)
            room.startListenUnread()
        }
    }


    override func prepareForReuse() {
        super.prepareForReuse()
        self.presenter = nil
        self.room = nil
        dispose = DisposeBag()
        badgeLbl?.text = ""
        if let room = room {
            room.stopListenUnread()
        }
    }

    var presenter: MessagePresenter? {
        didSet {
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearData()
        activityIndicator.stopAnimating()
        badgeLbl?.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func clearData() {
        iv_avatar?.image = nil
        lb_name?.text = " "
        lb_message?.text = " "
        lb_date?.text = " "
    }

}

extension MessageChannelTableViewCell: MessagePresenterView {
    func didSentMessage() {

    }

    func didReceivedNewMessage() {
        guard let message = presenter?.messages.last else {
            return
        }
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.lb_message.text = message.text
            self.lb_date.text = message.date.timeAgoDisplay()
            self.room?.lastDate = message.date
            self.room?.lastMessage = message.text
            self.delegate?.messageChannelTableViewCellDidReceiveNewRoom()
        }

    }

    override func showLoading() {
        activityIndicator.startAnimating()
    }

    override func hideLoading() {
        activityIndicator.stopAnimating()
    }
}
