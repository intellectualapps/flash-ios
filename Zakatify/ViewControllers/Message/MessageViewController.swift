//
//  MessageViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 10/6/17.
//  Copyright © 2017 Dht. All rights reserved.
//

private struct Constant {
    struct Dimension {
        static let kJSQMessagesCollectionViewCellLabelHeightDefault: CGFloat = 20.0
    }
}

import UIKit
import IQKeyboardManagerSwift

class MessageViewController: JSQMessagesViewController {
    var presenter: MessagePresenter? {
        didSet {
            guard let presenter = self.presenter else {
                return
            }
            self.title = presenter.otherUser.fullname
        }
    }
    
    override func senderId() -> String {
        return presenter!.otherUser.username
    }
    
    override func senderDisplayName() -> String {
        return presenter!.user.fullname
    }

    var outgoingBubbleImageData: JSQMessagesBubbleImage!
    var incomingBubbleImageData: JSQMessagesBubbleImage!
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputToolbar.contentView?.leftBarButtonItem = nil

        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
        }
        
        setupUI()
        setupObservable()
    }
    
    func setupUI() {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        outgoingBubbleImageData = bubbleFactory.outgoingMessagesBubbleImage(with: UIColor.white)
        incomingBubbleImageData = bubbleFactory.incomingMessagesBubbleImage(with: UIColor.white)
        
        self.inputToolbar.contentView?.textView?.placeHolder = "Send new message..."
        self.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = true
        
        let avatarSize = CGSize(width: 28, height: 28)
        
    
        self.collectionView?.collectionViewLayout.incomingAvatarViewSize = avatarSize
        self.collectionView?.collectionViewLayout.outgoingAvatarViewSize = avatarSize
        
        self.collectionView?.backgroundColor = UIColor.newGreenColor.withAlphaComponent(0.1)
    }
    
    func setupObservable() {
        self.inputToolbar.contentView?
            .textView?.rx.text.orEmpty
            .subscribe(onNext: { [weak self] text in
                self?.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = text != ""
        }).disposed(by: disposeBag)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if var insets = self.collectionView?.contentInset {
            insets.top = 0
            self.collectionView?.contentInset = insets
            self.collectionView?.scrollIndicatorInsets = insets
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
        presenter?.setOnline(status: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView?.collectionViewLayout.springinessEnabled = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        presenter?.setOnline(status: false)
    }

    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        presenter?.send(message: text)
        self.inputToolbar.contentView?.rightBarButtonItem?.isEnabled = true
        self.inputToolbar.contentView?.textView?.placeHolder = "Send new message..."
    }

    override func didPressAccessoryButton(_ sender: UIButton!) {

    }
    
    // MARK: - UICollectionView DataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.messages.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let presenter = presenter else {
            return UICollectionViewCell()
        }
        
        let message = presenter.messages[indexPath.item]
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        if message.senderId == self.senderId() {
            cell.textView?.textColor = .black
            cell.avatarImageView?.sd_setImage(with: presenter.otherUser.avartarURL)
        } else {
            cell.textView?.textColor = UIColor.newGreenColor
            cell.avatarImageView?.sd_setImage(with: presenter.user.avartarURL)
        }
        cell.avatarImageView?.clipsToBounds = true
        cell.avatarImageView?.layer.cornerRadius = 14.0
        cell.avatarImageView?.isHidden = false
        return cell
    }

    // MARK: - JSQMessages CollectionView DataSource
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData {
        return presenter!.messages[indexPath.item]
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = presenter!.messages[indexPath.item]
        message.senderId
        let senderId = self.senderId()
        
        if message.senderId == self.senderId() {
            return outgoingBubbleImageData
        }
        return incomingBubbleImageData
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabel AtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        let message = self.presenter!.messages[indexPath.item]
        if indexPath.item == 0 {
            return NSAttributedString(string: message.date.timeAgo())
        }
        if indexPath.item - 1 >= 0 {
            let previousMessage = presenter!.messages[indexPath.item - 1]
            if message.date.day != previousMessage.date.day {
                return NSAttributedString(string: message.date.timeAgo())
            }
        }
        return nil
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        guard let presenter = presenter else {
            return nil
        }
        let message = presenter.messages[indexPath.item]
        
        if message.senderId == self.senderId() {
            return NSAttributedString(string: self.senderId())
        }
        
//        if indexPath.item - 1 > 0 {
//            let previousMessage = presenter.messages[indexPath.item - 1]
//            if previousMessage.senderId == message.senderId {
//                return nil
//            }
//        }
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = presenter!.messages[indexPath.item]
        return NSAttributedString(string: message.date.toShortTime())
    }
    
    // MARK: - JSQMessages collection view flow layout delegate
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        let message = presenter!.messages[indexPath.item]
        if indexPath.item == 0 {
            return Constant.Dimension.kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        if indexPath.item - 1 >= 0 {
            let previousMessage = presenter!.messages[indexPath.item - 1]
            if message.date.day != previousMessage.date.day {
                return Constant.Dimension.kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let currentMessage = presenter!.messages[indexPath.row]
        if currentMessage.senderId == self.senderId() {
            return 20.0
        }
        if indexPath.item - 1 > 0 {
            let previousMessage = presenter!.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        return 20.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat {
        return 20.0
    }
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        collectionView?.contentInset.top = 0
    }

    override func textViewDidChange(_ textView: UITextView) {
        collectionView?.contentInset.top = 0
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        collectionView?.contentInset.top = 0
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        /*
         pres
         */
        if let currentMessage = presenter?.messages[indexPath.row] {
           presenter?.updateRead(msgid: currentMessage.id)
        }
    }
}

extension MessageViewController: MessagePresenterView {
    func didSentMessage() {
        self.finishSendingMessage(animated: true)
    }
    func didReceivedNewMessage() {
        DispatchQueue.main.async { [weak self] in
            self?.finishReceivingMessage()
        }
    }
}
