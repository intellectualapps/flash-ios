//
//  ProfileDetailsHeaderTableViewCell.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ProfileDetailsHeaderTableViewCell: UITableViewCell {
    static let reuseIdentifier = "ProfileDetailsHeaderTableViewCell"
    
    //
    @IBOutlet weak var iv_RateChart: CircleView!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_rateValue: UILabel!
    //
    @IBOutlet weak var lb_fullName: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    //
    @IBOutlet weak var lb_rankDescription: UILabel!
    @IBOutlet weak var bt_follow: Button!

    @IBOutlet weak var lb_portfolio: UILabel!
    @IBOutlet weak var lb_donations: UILabel!
    @IBOutlet weak var lb_followers: UILabel!

    var presenter: ZakatifierDetailsPresenter? {
        didSet {
            fillData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func pressFollow(_ sender: Any) {
        guard let presenter = presenter else {
            return
        }
        if presenter.model.isFollowing.state == true {
            presenter.unfollow()
        } else {
            presenter.follow()
        }
        presenter.refresh()
    }

    func fillData() {
        //
        guard let presenter = presenter else {
            return
        }
        if let url = presenter.avartarURL {
            iv_avatar.sd_setImage(with: url,
                                  placeholderImage: UIImage(named: "noAvatar"))
        }
        iv_RateChart.value = CGFloat(presenter.detailPercentCompleted)
        lb_rateValue.text = "\(Int(presenter.detailPercentCompleted))%"
        //
        lb_fullName.attributedText = presenter.nameAttributedString
        lb_username.text = "@" + presenter.username
        //
        lb_rankDescription.attributedText = presenter.rankDescriptionAttributedString
        //
        lb_portfolio.text = "\(presenter.model.counts.portfolioCount)"
        lb_donations.text = "\(presenter.model.counts.donationCount)"
        lb_followers.text = "\(presenter.model.counts.followerCount)"
        if presenter.model.isFollowing.state == true {
            bt_follow.setTitle("Unfollow", for: UIControlState.normal)
        } else {
            bt_follow.setTitle("Follow", for: UIControlState.normal)
        }

    }
    
    @IBAction func gotoMessage(_ sender: Any) {
        guard let user = UserManager.shared.currentUser,
            let otherUser = presenter?.model.user,
            let vc = UIStoryboard.message().instantiateViewController(withIdentifier: "MessageViewController") as? MessageViewController
            else {
                return
            }
        let chatPresenter = MessagePresenter(view: vc, user: user, otherUser: otherUser)
        vc.presenter = chatPresenter
        UIViewController.topMostViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
