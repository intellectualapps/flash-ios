//
//  DataServices.swift
//  Layout
//
//  Created by Trương Thắng on 5/27/17.
//  Copyright © 2017 Trương Thắng. All rights reserved.
//

import Foundation

class DataServices {
    static let shared: DataServices = DataServices()
    
    private var _usingGuides : [UsingGuide]?
    var usingGuides : [UsingGuide] {
        get{
            if _usingGuides == nil {
                updateusingGuide()
            }
            return _usingGuides ?? []
        }
        set{
            _usingGuides = newValue
        }
    }
    
    func updateusingGuide() {
        _usingGuides = []
        let model = UsingGuide(titleLabel: "ADD FAVORITE CHARITIES", imageIntro: #imageLiteral(resourceName: "Group"), descriptionLabel: "Enter your own Zakatify goal or we’ll help with our comprehensive calculator.")
        
        let model2 = UsingGuide(titleLabel: "ADD PAYMENT OPTIONS", imageIntro: #imageLiteral(resourceName: "onboarding-2"), descriptionLabel: "You can donate manually anytime, or automatically make monthly contributions to your portfolio. Make changes any time.")
        
        let model3 = UsingGuide(titleLabel: "ADD ZAKATIFY PREFERENCES", imageIntro: #imageLiteral(resourceName: "onboarding-1"), descriptionLabel: "Browse hundreds of charities and add as many as you want to your Zakatify portfolio. We’ll make suggestions based on your preferences. Modify your portfolio at any time.")
        
        let model4 = UsingGuide(titleLabel: "SHARE WITH ORTHER", imageIntro: #imageLiteral(resourceName: "onboarding-5"), descriptionLabel: "Grow your portfolio, share with friends, and increase your Zakatify score as you march towards your goal.")
        
        _usingGuides?.append(model)
        _usingGuides?.append(model2)
        _usingGuides?.append(model3)
        _usingGuides?.append(model4)
    }
    
}
