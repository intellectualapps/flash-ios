//
//  EmailRegisterViewController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class EmailContainerViewController: UIViewController {
    @IBOutlet weak var registerTabButton: UIButton!
    @IBOutlet weak var loginTabButton: UIButton!
    @IBOutlet weak var emphasizeView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet var alertView: UIVisualEffectView!
    
    var pageViewController: PageViewController!
    struct Text {
        static let registerTitle = "Register with Zakatify"
        static let loginTitle = "Login to Zakatify"
    }
    struct SegueIdentifier {
        static let embedPageVC = "embedPageVC"
        static let showForgotPassword = "showForgotPassword"
        static let showHomeVC = "ShowHomeVC"
        static let gotoIntroView = "gotoIntroView"

    }
    
    var atFirstTime = true
    
    var isRegisterMode = true {
        didSet {
            guard isRegisterMode != oldValue   else {return}
            confirmButton.setTitle((isRegisterMode ? Text.registerTitle : Text.loginTitle), for: .normal)
            if isRegisterMode {
                pageViewController.jump(toIndex: 0)
            } else {
                pageViewController.jump(toIndex: 1)
            }
        }
    }
    
    // MARK: life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if atFirstTime {
            UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseInOut, animations: {
                self.emphasizeView.frame = CGRect(x: self.registerTabButton.frame.minX,
                                                  y: self.registerTabButton.frame.maxY,
                                                  width: self.registerTabButton.frame.width,
                                                  height: 4)
            }, completion: nil)
            atFirstTime = false 
        }
        
    }
    
    @IBAction func aboutBtnAction(_ sender: Any) {
        openWebview(type: .about, title: "About")
    }
    
    @IBAction func onTapTermsAction(_ sender: UIButton) {
        openWebview(type: .terms, title: "Terms")
    }
    
    @IBAction func privacyBtnAction(_ sender: Any) {
         openWebview(type: .privacy, title: "Privacy")
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(showForgotPasswordVC), name: NotificationKey.showForgotPasswordVC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoHome), name: NotificationKey.gotoHome, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoIntroView), name: NotificationKey.gotoIntroView, object: nil)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case SegueIdentifier.embedPageVC:
            pageViewController = segue.destination as? PageViewController
            pageViewController.pageViewToContainerProtocol = self
        default:
            return
        }
    }
    
    // MARK: Action
    
    @IBAction func onClickTab(_ sender: UIButton) {
        
        isRegisterMode = sender === registerTabButton
        if sender ===  registerTabButton{
            registerTabButton.isSelected = true
            loginTabButton.isSelected = false
        } else {
            registerTabButton.isSelected = false
            loginTabButton.isSelected = true
        }
        UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseInOut, animations: {
            self.emphasizeView.frame = CGRect(x: sender.frame.minX,
                                              y: sender.frame.maxY,
                                              width: sender.frame.width,
                                              height: 4)
        }, completion: nil)
        
    }
    
    @IBAction func showArlert(_ sender: Any) {
        AlertView.shared?.toggle()
    }
    
    @IBAction func onClickConfirmButton() {
        if let controller = childViewControllers.filter({$0.isKind(of: PageViewController.self)}).first as? PageViewController {
            if let loginController = controller.currentViewController as? EmailLoginViewController {
                loginController.doLogin()
            }
            if let emailRegisterVC = controller.currentViewController as? EmailRegisterVC {
                emailRegisterVC.doRegister()
            }
        }
    }
    
    // MARK: FOLOW
    
    func showForgotPasswordVC() {
        performSegue(withIdentifier: SegueIdentifier.showForgotPassword, sender: nil)
    }

     func gotoHome() {
        performSegue(withIdentifier: SegueIdentifier.showHomeVC, sender: nil)
    }
    
     func gotoIntroView() {
        performSegue(withIdentifier: SegueIdentifier.gotoIntroView, sender: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - PageViewToContainerProtocol

extension EmailContainerViewController: PageViewToContainerProtocol {
    func pressTabButton() {
        if !isRegisterMode {
            onClickTab(registerTabButton)
        } else {
            onClickTab(loginTabButton)
        }
    }
}
