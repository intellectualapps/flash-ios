//
//  PreferencesViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class PreferencesViewController: UITableViewController, PreferenceView {
    enum Mode {
        case new
        case edit
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var tf_money: NumberTextFeild!
    @IBOutlet weak var iv_underline: UIImageView!
    @IBOutlet weak var iv_underline_leading: NSLayoutConstraint!
    @IBOutlet weak var bt_save: Button!
    
    // MARK: - Properties
    var disposeBag = DisposeBag()
    
    var mode: Mode = .new
    
    var z: NSNumber = NSNumber(value: 0) {
        didSet {
            guard let tf_money = tf_money else {
                return
            }
            tf_money.text = "\(z.floatValue)"
        }
    }
    
    var frequency: Zakat.frequency = .auto {
        didSet {
            guard let iv_underline_leading = iv_underline_leading , let iv_underline = iv_underline else {
                return
            }
            switch frequency {
            case .auto:
                iv_underline_leading.constant = 15
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            case .manual:
                iv_underline_leading.constant = 15 + 8 + iv_underline.frame.size.width
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    var presenter: PreferencePresenter?

    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIViewController.logEvent(eventName: .signupPreferences)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.addBackButtonDefault()
        self.addSwipeRight()
        
        presenter = PreferencePresenter(view: self)
        
        setupUI()
        setupObservable()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let footer = tableView.tableFooterView , let window = AppDelegate.shareInstance().window else {
            return
        }
        let windowFrame = window.frame
        let frameInWindow = tableView.convert(footer.frame, to: window)
        var height = windowFrame.size.height - frameInWindow.origin.y
        if height <= 60 {
            height = 60
        }
        footer.frame.size.height = height
    }
    
    func setupUI() {
        tf_money.text = "0"
        tf_money.delegate = self
        
        switch mode {
        case .new:
            bt_save.setTitle("Next: Add payment options", for: UIControlState.normal)
        case .edit:
            bt_save.setTitle("Save changes", for: UIControlState.normal)
        }
    }
    
    func setupObservable() {
        tf_money.rx.controlEvent(UIControlEvents.editingChanged).bind { [unowned self] () in
            if let number = self.tf_money.number {
                self.presenter?.zakat.amount = number.floatValue
            }
            }.addDisposableTo(disposeBag)
    }

    // MARK: - Actions
    @IBAction func next(_ sender: Any) {

        //Log event
        let value = Float(tf_money.number ?? 0)
        let frequency = (self.frequency == Zakat.frequency.auto) ? "Automatic" : "Manually"
        let params: AppEvent.ParametersDictionary = [
            AppEventParameterName.custom("Frequency"): frequency,
            AppEventParameterName.custom("User_total"): String(value)
        ]
        UIViewController.logEvent(eventName: .preferencesSave, parameters: params)

        presenter?.addZakatGoal(amount: value)
    }
    @IBAction func selectAuto(_ sender: Any) {
        self.frequency = .auto
        presenter?.frequency = .auto
    }
    @IBAction func selectManual(_ sender: Any) {
        self.frequency = .manual
        presenter?.frequency = .manual
    }
    
    // MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let zakat = presenter?.zakat.copy() else {
            return
        }
        if segue.identifier == "calculator" {
            //Log event
            let value = tf_money.text ?? ""
            let params: AppEvent.ParametersDictionary = [AppEventParameterName.custom("value"): value]
            UIViewController.logEvent(eventName: .signupCalculator, parameters: params)

            if let navigation = segue.destination as? UINavigationController {
                if let vc = navigation.viewControllers.first as? CalculateGoalViewController {
                    vc.zakat = zakat
                    vc.delegate = self
                }
            }
        }
    }
    
    // MARK: - Methods
    func fillData() {
        guard let presenter = presenter else {
            return
        }
        frequency = presenter.frequency
        z = presenter.z
    }
    
    func saveSuccess() {
        switch mode {
        case .new:
            self.performSegue(withIdentifier: "payment", sender: nil)
        case .edit:
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let footer = tableView.tableFooterView , let window = AppDelegate.shareInstance().window else {
            return
        }
        let windowFrame = window.frame
        let frameInWindow = tableView.convert(footer.frame, to: window)
        var height = windowFrame.size.height - frameInWindow.origin.y
        if height <= 60 {
            height = 60
        }
        footer.frame.size.height = height
    }
    
    override func back() {
        UIViewController.logEvent(eventName: .singupCalculatorBack)
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - CalculateGoalViewControllerDelegate
extension PreferencesViewController: CalculateGoalViewControllerDelegate {
    func didCalculateZ(zakat: Zakat) {
        presenter?.zakat = zakat
    }
    
    func bindingMoney(value: String?) {
        self.tf_money.text = value
        let amount = value ?? "0"
        presenter?.zakat.amount = Float(amount) ?? 0
    }
}

// MARK: - UITextFieldDelegate
extension PreferencesViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let number = Int(textField.text!.replacingOccurrences(of: ",", with: "")), number >= 10000 {
            let alert = UIAlertController(title: nil, message: "You've set a donation goal of over $\(textField.text!) for this year. Are you sure you want to do this", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: { [weak self] _ in
                self?.presenter!.z = 0
            })
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
}
