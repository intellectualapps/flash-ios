//
//  ChoseTagsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ChoseTagsViewController: UIViewController, CategoryView {
    @IBOutlet weak var uv_Categories: TagListView! {
        didSet {
            uv_Categories?.delegate = self
            uv_Categories.alignment = .center
        }
    }

    var presenter: CategoryPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = CategoryPresenter(view: self)
        addBackButtonDefault()
        self.addSwipeRight()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.getAllCategory(completion: {
        })
    }
    
    func categoryFillData() {
        guard let presenter = presenter,
        let uv_Categories = uv_Categories else {
            return
        }
        uv_Categories.removeAllTags()
        let tags = presenter.categories.map { (category) in
            return category.description
        }
        uv_Categories.addTags(tags)
        let tagsView = uv_Categories.allTagsView()
        let selectedTags = presenter.userCategories.map { (category) in
            return category.description
        }
        let tagsViewNeedSelect = tagsView.filter { (tagview) -> Bool in
            if let title = tagview.currentTitle {
                return selectedTags.contains(title)
            }
            return false
        }
        
        DispatchQueue.main.async {
            for tagView in tagsViewNeedSelect {
                tagView.isSelected = true
            }
        }
    }
    
    func added() {
        guard let presenter = presenter else {
            return
        }
        if presenter.userCategories.count > 0 {
            AppDelegate.shareInstance().gotoHomeViewController()
        }
    }
    
    @IBAction func next(_ sender: Any) {
        guard let presenter = presenter else {
            return
        }
        let selectedTags = uv_Categories.selectedTags()
        let params: AppEvent.ParametersDictionary = [
            AppEventParameterName.custom("number_of_interest"): String(selectedTags.count)
        ]
        UIViewController.logEvent(eventName: .signupReady, parameters: params)
        
        let tagDescriptions = selectedTags.map { (tagview) in
            return tagview.currentTitle ?? ""
        }
        
        presenter.add(categoryDescriptions: tagDescriptions)
    }

}

extension ChoseTagsViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        tagView.isSelected = !tagView.isSelected
    }
}
