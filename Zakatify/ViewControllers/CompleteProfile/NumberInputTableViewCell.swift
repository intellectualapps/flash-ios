//
//  NumberInputTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import AMPopTip

protocol NumberInputTableViewCellDelete: class {
    func selectToolTipButton(cell:NumberInputTableViewCell)
}

class NumberInputTableViewCell: UITableViewCell {
    static var identifier: String = "NumberInputTableViewCell"
    var disposeBag = DisposeBag()
    var bt: UIButton!
    
    weak var delegate:NumberInputTableViewCellDelete?
    var toolTipView: ToolTipView!
    var titleText = ""
    var contentText = ""
    let popTip = PopTip()
    var direction = PopTipDirection.left
    var topRightDirection = PopTipDirection.down
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tf_input: NumberTextFeild! {
        didSet {
            guard let tf_input = tf_input else {
                return
            }
            tf_input.rx.controlEvent(UIControlEvents.editingChanged).bind { [unowned self] (event) in
                guard let data = self.data else {
                    return
                }
                guard let zakat = self.zakat else {
                    return
                }
                guard let number = tf_input.number else {
                    return
                }
                switch data {
                case .cash:
                    zakat.ce = number.floatValue
                case .gold:
                    zakat.gs = number.floatValue
                case .real:
                    zakat.re = number.floatValue
                case .inves:
                    zakat.i = number.floatValue
                case .personal:
                    zakat.pu = number.floatValue
                case .liabiliti:
                    zakat.l = number.floatValue
                }
            }.addDisposableTo(disposeBag)
        }
    }
    
    var data: CalculateGoalViewController.CellData? {
        didSet {
            guard let data = data else {
                return
            }
            getTitleAndContent(data)
//            tf_input.placeholder = data.placesHoldle
            titleLabel.text = data.placesHoldle
            guard let zakat = zakat else {
                return
            }
            var value: Float
            switch data {
            case .cash:
                value = zakat.ce
            case .gold:
                value = zakat.gs
            case .real:
                value = zakat.re
            case .inves:
                value = zakat.i
            case .personal:
                value = zakat.pu
            case .liabiliti:
                value = zakat.l
            }
            let number = NSNumber(value: value)
            tf_input.text = "\(number.intValue)"
        }
    }
    
    var zakat: Zakat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        toolTipView = ToolTipView.fromNib()
        bt = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        bt.setImage(#imageLiteral(resourceName: "ic-question"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.selectQuestion), for: UIControlEvents.touchUpInside)
        tf_input.rightView = bt
        tf_input.rightViewMode = .always
        popTip.cornerRadius = 10
        popTip.arrowSize = CGSize(width: 15, height: 10)
        popTip.shouldShowMask = true
        popTip.bubbleColor = UIColor.white
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func selectQuestion() {
        delegate?.selectToolTipButton(cell: self)
        toolTipView.frame = CGRect(x: 0, y: 0, width: 226, height: 278)
        toolTipView.titleView.text = titleText
        toolTipView.content.text = contentText
        let frame = bt.superview?.convert(bt.frame, to: AppDelegate.shareInstance().window)
        popTip.show(customView: toolTipView, direction: .left, in: AppDelegate.shareInstance().window!, from: frame!)

    }
    
    func getTitleAndContent(_ data: CalculateGoalViewController.CellData) {
        switch data {
        case .cash:
            titleText = "Cash & Equivalents"
            contentText = "Cash & Equivalents are liquid assets (cash on hand, bank accounts, business liquid assets on pro rata ownership basis, payments owed, etc.) from any source. Must be within your control."
        case .gold:
            titleText = "Gold & Silver"
            contentText = "Gold & Silver refers to relatively pure gold and silver in your possession (including jewelry) as well as other precious metals held as readily convertible investments"
        case .real:
            titleText = "Real Estate"
            contentText = "Real Estate includes rental income and any income made from real estate sales made during the year. Does not include the value of your personal home."
        case .inves:
            titleText = "Investments"
            contentText = "This category includes stocks, bonds, savings certificates, investment commodities, trusts and partnerships – usually total value. When actively trading (i.e., working capital), only gains should go here. When held as long-term investments, you may either take your pro rata value of business assets (cash, receivables &amp; inventory) or simply list the total market value as of today’s date.\n\nThis also applies to retirement accounts – defined contribution pensions, IRAs, 401Ks or equivalents, etc. Subtract any early-withdrawal penalties (typically 10% if you have not reached withdrawal eligibility) and taxes (usually taxed at your marginal personal rate). Do not count employer match if not yet vested or accessible.\n\nEducation and unused health savings accounts should also be counted in the same manner. Other property purchased with intent to resell – e.g., art as an investment, etc., as well.\n\nFor business inventory, count your work in progress (at cost) and raw materials (at cost), pro rata in line with your ownership stake. Value your inventory at the first sale price – retail value if you are a retailer, wholesale if you are a wholesaler"
        case .personal:
            titleText = "Personal Use"
            contentText = "Personal Use includes the monthly money you need for clothing, food, shelter, household furniture, utensils/china, cars/transportation, medical expenses, cell phone bill, etc."
        case .liabiliti:
            titleText = "Liabilities"
            contentText = "Liabilities include bills due immediately for your home (i.e., monthly payment or rent), credit card bills, utilities, insurance payments, loan payments, bad debts (on loans you have made to others), and pro rata business liabilities."
        }
    }
}
