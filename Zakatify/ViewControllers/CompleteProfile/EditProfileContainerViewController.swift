//
//  EditProfileContainerViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/17/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class EditProfileContainerViewController: UIViewController {
    
    var editProfileViewController:EditProfileViewController? {
        didSet {
            editProfileViewController?.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 20))
            editProfileViewController?.container = self
        }
    }
    var percentCompleted: Double = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addSwipeRight()
        self.addBackButtonDefault()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickSave(_ sender: Any) {
        editProfileViewController?.next(sender)
    }

    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let editProfileViewController = segue.destination as? EditProfileViewController {
            editProfileViewController.percentCompleted = percentCompleted
            self.editProfileViewController = editProfileViewController
        }
    }
    

}

