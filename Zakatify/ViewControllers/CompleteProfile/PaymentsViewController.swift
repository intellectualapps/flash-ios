//
//  PaymentsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class PaymentsViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var iv_payment: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bt_next: Button!
    @IBOutlet weak var bt_bottomSpace_addCrediCard: NSLayoutConstraint!
    
    // MARK: - Properties
    enum Mode {
        case new
        case edit
    }
    
    var mode: Mode = .new {
        didSet {
            switch mode {
            case .new:
                bt_next?.isHidden = false
                bt_bottomSpace_addCrediCard?.constant = 90
            case .edit:
                bt_next?.isHidden = true
                bt_bottomSpace_addCrediCard?.constant = 25
            }
            self.view.layoutIfNeeded()
        }
    }
    
    var didGetPaymentList = false
    
    var refTableView: UITableView? {
        return tableView
    }
    
    var presenter: CreditCardViewPresenter?
    var refPresenter: TableViewPresenter? {
        return presenter
    }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) {[weak self] (notification) in
            self?.presenter?.refresh()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didGetPaymentList {
            didGetPaymentList = true
            presenter?.refresh()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Methods
    func setupUI() {
        self.addBackButtonDefault()
        self.addSwipeRight()
        let mode = self.mode
        self.mode = mode
        presenter = CreditCardPresenter(view: self)
        
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "PaymentTableViewCell", bundle: nil) , forCellReuseIdentifier: PaymentTableViewCell.identifier)
    }
    
    func loadingMore() {
        
    }
    
    func loadedMore() {
        
    }
    
    func insert(indexPaths: [IndexPath]) {
        
    }
    
    func refreshing() {
        showLoading()
    }
    
    func refreshed() {
        DispatchQueue.main.async {[weak self] in
            guard let `self` = self else {
                return
            }
            self.hideLoading()
            guard let presenter = self.presenter else {
                self.iv_payment.isHidden = false
                return
            }
            if presenter.numberOfRowsInSection(section: 0) == 0 {
                self.iv_payment.isHidden = false
            } else {
                self.iv_payment.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - IBActions
    @IBAction func addPayment(_ sender: Any) {
        //New requirement 10/02/2018 => open safari
        UIApplication.shared.connectOrCreatePaypalAccount()
    }
    
    @IBAction func next(_ sender: Any) {
        guard let presenter = presenter else {
            return
        }
        UIViewController.logEvent(eventName: .signupIntrest)
        self.performSegue(withIdentifier: "tag", sender: nil)
    }
}

// MARK: - UITableViewDataSource
extension PaymentsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentTableViewCell.identifier, for: indexPath) as? PaymentTableViewCell else {
            return UITableViewCell()
        }
        
        let payment = presenter?.paymentAt(indexPath: indexPath)
        cell.payment = payment
        cell.delegate = self
        return cell
    }
}

// MARK: - UITableViewDelegate

extension PaymentsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableHeaderView = UIView.loadFromNibNamed(nibNamed: "PaymentsHeaderView")
        tableHeaderView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 80)
        return tableHeaderView
    }
}

// MARK: - CreditCardView
extension PaymentsViewController: CreditCardView {
    func saveSuccess() {
        guard let presenter = presenter else {
            iv_payment.isHidden = false
            return
        }
        let rowCount = presenter.numberOfRowsInSection(section: 0)
        iv_payment.isHidden = rowCount > 0
        tableView.reloadData()
    }
}

extension PaymentsViewController: PaymentTableViewCellDelegate {
    func clickMakePrimary(payment: Payment?, cell: PaymentTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        presenter?.makePrimary(index: indexPath)
    }
    
    func clickDelete(payment: Payment?, cell: PaymentTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        presenter?.delete(index: indexPath)
    }
}
