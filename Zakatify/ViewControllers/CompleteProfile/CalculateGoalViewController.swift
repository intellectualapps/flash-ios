//
//  CalculateGoalViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import PagingMenuController
import RxSwift

class PageController: PagingMenuController {
    var cPage = Variable(0)
    override func move(toPage page: Int, animated: Bool) {
        super.move(toPage: page, animated: animated)
        self.cPage.value = page
    }
}

protocol CalculateGoalViewControllerDelegate: class {
    func didCalculateZ(zakat:Zakat)
    func bindingMoney(value: String?)
}

class CalculateGoalViewController: UIViewController {
    let dispose = DisposeBag()
    enum CellData {
        case cash
        case gold
        case real
        case inves
        case personal
        case liabiliti
        var placesHoldle: String {
            switch self {
            case .cash:
                return "Cash and equivalents"
            case .gold:
                return "Gold and silver"
            case .real:
                return "Real estate"
            case .inves:
                return "Investments"
            case .personal:
                return "Personal use"
            case .liabiliti:
                return "Liabilities"
            }
        }
        var key: String {
            switch self {
            case .cash:
                return "ce"
            case .gold:
                return "gs"
            case .real:
                return "re"
            case .inves:
                return "i"
            case .personal:
                return "pu"
            case .liabiliti:
                return "l"
            }
        }
        
        var description: String {
            return ""
        }
    }
    @IBOutlet weak var tf_goal: UITextField!
    @IBOutlet weak var uv_container: UIView!
    var pageController: PageController!
    weak var delegate: CalculateGoalViewControllerDelegate?
    var zakat: Zakat?
    var goalNumber: NSNumber?
    var nw: NSNumber = NSNumber(value: 0)
    var z: NSNumber = NSNumber(value: 0) {
        didSet {
            guard let tf_goal = tf_goal else {
                return
            }
            tf_goal.text = "\(z.floatValue)"
        }
    }
    
    private let viewController1 = WealthTableViewController(style: UITableViewStyle.plain)
    private let viewController2 = DeductionsTableViewController(style: UITableViewStyle.plain)
    
    private struct PagingMenuOptions: PagingMenuControllerCustomizable {
        
        fileprivate var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
        }
        
        var pagingControllers: [UIViewController]
        
        fileprivate struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .segmentedControl
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItem1(), MenuItem2()]
            }
            var focusMode: MenuFocusMode {
                return .underline(height: 3, color: UIColor.navigationBartinColor, horizontalPadding: 15, verticalPadding: 0)
            }
        }
        
        fileprivate struct MenuItem1: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: "WEALTH"))
            }
        }
        fileprivate struct MenuItem2: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: "DEDUCTIONS"))
            }
        }
        
        init(controllers:[UIViewController]) {
            pagingControllers = controllers
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewController1.zakat = zakat
        viewController2.zakat = zakat
        let options = PagingMenuOptions(controllers: [viewController1, viewController2])

        pageController = PageController(options: options)
        pageController.view.frame = uv_container.bounds
        addChildViewController(pageController)
        uv_container.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)

        guard let zakat = zakat else {
            return
        }
        self.z = NSNumber(value: zakat.amount)
        //add observer
        pageController.cPage.asObservable()
            .subscribe(onNext: {[weak self] (value) in
                if value == 1 {
                    UIViewController.logEvent(eventName: .singupDeductionTab)
                }
            }).disposed(by: dispose)

        tf_goal.delegate = self
    }
    
    func getDefaultValue() {
        self.showLoading()
        let service = GoldValueServicesCenter()
        service.getDefaultValue { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideLoading()
            switch result {
            case .success(let value):
                let number = NSNumber(value: value)
                strongSelf.goalNumber = number
                self?.calculate(self as Any)
            case .failure(let error):
                strongSelf.showAlert(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func calculate(_ sender: Any) {
        guard let goalNumber = goalNumber else {
            getDefaultValue()
            return
        }
        self.view.endEditing(true)

        zakat?.calculate(n: goalNumber.floatValue)
        guard let zakat = zakat else {
            return
        }
        tf_goal.text = String(zakat.amount)
        let params: AppEvent.ParametersDictionary = [
            AppEventParameterName.custom("Cash value"): String(zakat.ce),
            AppEventParameterName.custom("Gold value"): String(zakat.gs),
            AppEventParameterName.custom("Real estate"): String(zakat.re),
            AppEventParameterName.custom("Investment"): String(zakat.i),
            AppEventParameterName.custom("Personal use"): String(zakat.pu),
            AppEventParameterName.custom("Liabilities"): String(zakat.l),
            AppEventParameterName.custom("Total"): String(goalNumber.floatValue)
        ]
        UIViewController.logEvent(eventName: .signupCalulate, parameters: params)

        delegate?.didCalculateZ(zakat: zakat)
    }
    @IBAction func clearZ(_ sender: Any) {
        z = NSNumber(value: 0)
    }
    
    @IBAction func back(_ sender: Any) {
        //LogEvent
        UIViewController.logEvent(eventName: .singupCalculatorBack)
        delegate?.bindingMoney(value: tf_goal?.text)
        self.dismiss(animated: true, completion: nil)
    }

}

extension CalculateGoalViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let number = Int(textField.text!.replacingOccurrences(of: ",", with: "")), number > 10000 {
            let alert = UIAlertController(title: nil, message: "Are you sure to donate $\(number)", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: { _ in
                textField.text = "0"
            })
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
}
