//
//  TextField.swift
//  Zakatify
//
//  Created by Thang Truong on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift

protocol TextFieldDelegate: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField)
}
@IBDesignable
class TextField: UIView {
    let disposeBag = DisposeBag()
    var labelHeigh : CGFloat = 20

    @IBOutlet weak var label: UILabel! {
        didSet {
            heighTextConstraint = label.heightAnchor.constraint(equalToConstant: 0)
            heighTextConstraint.isActive = true
        }
    }
    var isShowTitle = false {
        didSet {
            UIView.animate(withDuration: 0.35) {
                self.heighTextConstraint.constant = self.isShowTitle ? self.labelHeigh : 0
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var seperatorView: UIView! {
        didSet {
            seperatorView?.backgroundColor = UIColor.groupTableViewBackground
        }
    }
    var heighTextConstraint: NSLayoutConstraint!
    @IBInspectable
    var isPasswordTextField : Bool = true
    
    @IBInspectable
    
    weak var delegate: UITextFieldDelegate?
    @IBOutlet var showButton : UIButton? {
        didSet {
            showButton?.addTarget(self, action: #selector(toggleShowPassword), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var textField: UITextField! {
        didSet {
            
            textField.rx.controlEvent(UIControlEvents.editingChanged).bind { [unowned self] in
                guard let text = self.textField.text else {
                    self.layoutIfNeeded()
                    self.isShowTitle = false
                    return
                }
                if text.isEmpty {
                    self.isShowTitle = false
                } else {
                    self.isShowTitle = true
                }
                self.layoutIfNeeded()
                }.addDisposableTo(disposeBag)
            
            
            
            textField.delegate = self
//            textField.rightView = showButton
            if isPasswordTextField {
                textField?.rightViewMode = .always
            } else {
                textField?.rightViewMode = .never
            }
            textField.isSecureTextEntry = isPasswordTextField
            textField.font = UIFont.systemFont(ofSize: 17)
            
        }
    }
    
    func toggleShowPassword() {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        showButton?.setTitle(textField.isSecureTextEntry ? "Show": "Hide", for: .normal)
    }

}

extension TextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing?(textField)
        isShowTitle = true
        seperatorView?.backgroundColor = UIColor.orange
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing?(textField)
        guard let text = textField.text else {
            self.isShowTitle = false
            self.layoutIfNeeded()
            return
        }
        if text.isEmpty {
            self.isShowTitle = false
        } else {
            self.isShowTitle = true
        }
        self.layoutIfNeeded()
        seperatorView?.backgroundColor = UIColor.groupTableViewBackground

    }
}
