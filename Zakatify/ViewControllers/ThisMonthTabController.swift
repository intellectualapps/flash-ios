//
//  ThisMonthTabController.swift
//  Zakatify
//
//  Created by center12 on 5/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ThisMonthTabController: UIViewController , ListZakatifiersView{
    //MARK: Data Models
    //-----------------------
    
    //MARK: Local Variables
    //-----------------------
    var disposed = DisposeBag()
    var refTableView: UITableView? {
        return monthTableView
    }
    
    var presenter: ListMonthZakatifiersPresenter?
    var refPresenter: TableViewPresenter? {
        return presenter
    }
    //MARK: UI Elements
    //-----------------------
    @IBOutlet weak var monthTableView: UITableView! {
        didSet {
            monthTableView?.dataSource = self
            monthTableView?.delegate = self
        }
    }
    
    //MARK: UIViewController
    //-----------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            monthTableView.refreshControl = refreshControl
        } else {
            monthTableView.backgroundView = refreshControl
        }
        monthTableView.register(UINib(nibName: "ZakatifierTableViewCell", bundle: nil), forCellReuseIdentifier: ZakatifierTableViewCell.identifier)
        
        self.monthTableView.estimatedRowHeight = 100
        self.monthTableView.rowHeight = UITableViewAutomaticDimension
        //
        presenter = ListMonthZakatifiersPresenter(view: self)
        presenter?.refresh()
        self.monthTableView?.tableFooterView = UITableViewHeaderFooterView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //MARK: View Setup
    //-----------------------
    
    //MARK: Events
    //-----------------------
    func reload() {
        DispatchQueue.main.async {
            self.monthTableView?.reloadData()
        }
    }
    
    func refreshControlChange() {
        presenter?.refresh()
    }
    
    func getUsersFromServer() {
        presenter?.refresh()
    }
}

//MARK: Extension UITableViewDelegate
//-----------------------
extension ThisMonthTabController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard.zakatifier().instantiateViewController(withIdentifier: "ProfileDetailsViewController") as? ProfileDetailsViewController else {
            return
        }
        guard let model = presenter?.zakatifer(at: indexPath) else { return }
        vc.presenter = ZakatifierDetailsPresenter(view: vc, model: model)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: Extension UITableViewDataSource
//-----------------------
extension ThisMonthTabController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ZakatifierTableViewCell.identifier, for: indexPath) as? ZakatifierTableViewCell else {
            return UITableViewCell()
        }
        
        // Configure the cell...
        if let zakatifier = presenter?.zakatifer(at: indexPath) {
            cell.zakatifier = zakatifier
        }
        cell.clickAddBlock = { [weak self] zakatifier in
            guard let zakatifier = zakatifier else { return }
            self?.presenter?.addZakatifier(zakatifier)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastElement - 1 {
            presenter?.loadmore()
        }
    }
    
    
}
