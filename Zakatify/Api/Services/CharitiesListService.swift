//
//  CharitiesListService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/9/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CharitiesListService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results: CharitiesInfo?
        if let root = response as? [String: Any] {
            results = CharitiesInfo(dict: root)
        }
        super.onFinish(results, error: error, completion: completion)
    }
}
