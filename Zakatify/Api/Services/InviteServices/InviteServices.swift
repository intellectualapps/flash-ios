//
//  ZakatServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result


protocol InviteServices {
    func userInvitations(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(invitations:[Invitation],page:Int,size:Int,total:Int),NSError>)-> Void)
    func sendInvitation(username: String, inviteeEmail: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}

class InviteServicesCenter: InviteServices {
    func userInvitations(username: String, page: Int, size: Int, complete: @escaping (Result<(invitations: [Invitation], page: Int, size: Int, total: Int), NSError>) -> Void) {
        InviteAPIProvider.request(.userInvitations(username: username, page: page, size: size)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let invitations: [Invitation] = try filtedResponce.mapArray(path: "invitations")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = try filtedResponce.mapValue(path: "totalCount")
                    let result = (invitations: invitations, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func sendInvitation(username: String, inviteeEmail: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        InviteAPIProvider.request(.sendInvitation(username: username, inviteeEmail: inviteeEmail)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let state: Bool = try filtedResponce.mapValue(path: "state")
                    complete(Result.success(state))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
