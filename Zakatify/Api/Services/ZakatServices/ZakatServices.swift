//
//  ZakatServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result


protocol ZakatServices {
    func add(zakat:Zakat, username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void)
    func update(zakat:Zakat, username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void)
    func get(username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void)
    func delete(username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}

class ZakatServicesCenter: ZakatServices {
    func add(zakat:Zakat, username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void) {
        let json = ["amount":zakat.amount,
                    "frequency-id":zakat.frequencyId,
                    "period":Date(),
                    "cash":zakat.ce,
                    "gold":zakat.gs,
                    "real-estate":zakat.re,
                    "investment":zakat.i,
                    "personal-use":zakat.pu,
                    "liability":zakat.l] as [String : Any]
        ZakatAPIProvider.request(ZakatAPI.add(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let zakat:Zakat = try response.mapObject()
                    complete(Result.success(zakat))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func update(zakat:Zakat, username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void) {
        let json = ["amount":zakat.amount,
                    "frequency-id":zakat.frequencyId,
                    "period":Date(),
                    "cash":zakat.ce,
                    "gold":zakat.gs,
                    "real-estate":zakat.re,
                    "investment":zakat.i,
                    "personal-use":zakat.pu,
                    "liability":zakat.l] as [String : Any]
        ZakatAPIProvider.request(ZakatAPI.update(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let zakat:Zakat = try response.mapObject()
                    complete(Result.success(zakat))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func get(username: String, complete:@escaping (_ result:Result<Zakat,NSError>)-> Void) {
        ZakatAPIProvider.request(ZakatAPI.get(username: username))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let zakat:Zakat = try response.mapObject()
                    complete(Result.success(zakat))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func delete(username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        ZakatAPIProvider.request(ZakatAPI.delete(username: username))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["status"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
