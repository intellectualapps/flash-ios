//
//  UserSevicesEndPoint.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let ZakatifierAPIProvider = MoyaProvider<ZakatifierAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum ZakatifierAPI {
    case topZakatifiers
    case topMonthZakatifiers
    case userDetails(user: String)
    case userReviews(user: String, page: Int, size: Int)
    case follow(user: String, followed: String)
    case unfollow(user: String, followed: String)
}

extension ZakatifierAPI: TargetType {
    public var headers: [String : String]? {
        return nil
    }
    public var baseURL: URL {
        switch self {
        case .userDetails:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/user/public")!
        case .userReviews:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/review/user")!
        case .follow:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/follower")!
        case .unfollow:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/follower")!
        default:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/zakatifier")!

        }
    }
    public var path: String {
        switch self {
        case .topMonthZakatifiers:
            return "/current-month/top"
        case .topZakatifiers:
            return "/top"
        case .userDetails(user: let username):
            return "/username/\(username)"
        case .userReviews(user: let username, page: _, size: _):
            return "/\(username)"
        case .follow:
            return ""
        case .unfollow:
            return ""
        }
    }
    public var method: Moya.Method {
        switch self {
        case .topZakatifiers:
            return .get
        case .topMonthZakatifiers:
            return .get
        case .userDetails(user: _):
            return .get
        case .userReviews(user: _, page: _, size: _):
            return .get
        case .follow(user: _, followed: _):
            return .post
        case .unfollow(user: _, followed: _):
            return .delete
        }
    }
    public var parameters: [String: Any]? {
        switch self {
        case .topZakatifiers:
            return nil
        case .topMonthZakatifiers:
            return nil
        case .userDetails(user: _):
            return nil
        case .userReviews(user: _, page: let page, size: let size):
            return ["page-number":page,"page-size":size]
        case .follow(user: let username, followed: let followed):
            return ["follower": username, "followed": followed]
        case .unfollow(user: let username, followed: let followed):
            return ["follower": username, "followed": followed]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
         return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}

private func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}
