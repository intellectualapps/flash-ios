//
//  ZakatAPI.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let chatAPIProvider = MoyaProvider<ChatAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                                    responseDataFormatter: JSONResponseDataFormatter),
                                                                AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

enum ChatAPI {
    case getRoomID(username: String, username: String)
    case messageList(username: String)
    case push(username: String, receiver: String, connectionId: String)
}

extension ChatAPI: TargetType {
    var headers: [String : String]? {
        return nil
    }

    public var baseURL: URL { return URL(string: "https://flash-bff-ios.appspot.com/api/v1/message")! }
    public var path: String {
        switch self {
        case .getRoomID(username: let username, username: _):
            return "/\(username)"
        case .messageList(username: let username):
            return "/list/\(username)"
        case .push:
            return "/notify"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getRoomID:
            return .get
        case .messageList:
            return .get
        case .push:
            return .post
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .getRoomID(username: _, username: let username):
            return ["other-participant": username]
        case .messageList:
            return nil
        case .push(username: let username, receiver: let receiver, connectionId: let connectionId):
            return ["online-username": username, "offline-username": receiver, "message-id": connectionId]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}
