//
//  AccessTokenPlugin.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

struct AccessTokenPlugin: PluginType {
    private var authVal: String {
        var token = ""
        if let authenToken = UserManager.shared.authenToken {
            token = authenToken
        }
        return "Bearer " + token
    }
    
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        if let authorizable = target as? AccessTokenAuthorizable, authorizable.authorizationType == .none {
            return request
        }
        
        var request = request
        request.addValue(authVal, forHTTPHeaderField: "Authorization")
        
        return request
    }
}
