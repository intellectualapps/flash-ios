//
//  UserServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result

protocol GoalValueServices {
    func getDefaultValue(complete:@escaping (_ result:Result<Double,NSError>)-> Void)
}


class GoldValueServicesCenter: GoalValueServices {
    func getDefaultValue(complete: @escaping (Result<Double, NSError>) -> Void) {
        GoalValueAPIProvider.request(GoalValueAPI.getDefaultValue) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let value = json["value"] as? Double {
                            complete(Result.success(value))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
