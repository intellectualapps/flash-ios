//
//  CharityDetailService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
class CharityDetailService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var charity: Charity?
        if let root = response as? [String: Any] {
            charity = Charity(JSON: root)
        }
        super.onFinish(charity, error: error, completion: completion)
    }
}
