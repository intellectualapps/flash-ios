//
//  UserSevicesEndPoint.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let UserAPIProvider = MoyaProvider<UserAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum UserAPI {
    enum AuthenType:String {
        case email
        case facebook
        case twitter
    }
    
    case register(username:String,email:String,password:String)
    case verify(userName:String)
    case authenticate(id:String,password:String,type:String)
    case forgotPassword(email:String)
    case updateUser(json:[String:Any])
    case createUser(json:[String:Any])
    case sideMenu(user:String)
    case deviceToken(deviceToken: String, username: String)
}

extension UserAPI: TargetType {
    public var headers: [String : String]? {
        return nil
    }
    public var baseURL: URL { return URL(string: "https://flash-bff-ios.appspot.com/api/v1/user")! }
    public var path: String {
        switch self {
        case .register:
            return ""
        case .verify(let userName):
            return "/verify/\(userName.urlEscaped)"
        case .authenticate:
            return "/authenticate"
        case .forgotPassword:
            return "/password"
        case .updateUser:
            return "/profile"
        case .createUser:
            return "/username"
        case .sideMenu(user: let username):
            return "/public/username/\(username)"
        case .deviceToken(deviceToken: _, username: let username):
            return "/device-token/\(username)"
        }
    }
    public var method: Moya.Method {
        switch self {
        case .register:
            return .post
        case .verify:
            return .get
        case .authenticate:
            return .post
        case .forgotPassword:
            return .post
        case .updateUser:
            return .put
        case .createUser:
            return .post
        case .sideMenu:
            return .get
        case .deviceToken:
            return .post
        }
    }
    public var parameters: [String: Any]? {
        switch self {
        case .register(let userName, let email, let password):
            return ["username":userName, "email":email, "password":password]
        case .verify:
            return nil
        case .authenticate(let id, let password, let type):
            return ["id":id, "password":password, "auth-type":type]
        case .forgotPassword(let email):
            return ["email":email]
        case .updateUser(let json):
            return json
        case .createUser(json: let json):
            return json
        case .sideMenu(user: let _):
            return nil
        case .deviceToken(deviceToken: let token, username: _):
            return ["token": token, "platform": 2, "uuid": UIDevice.current.identifierForVendor!.uuidString]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
         return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}

private func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}
