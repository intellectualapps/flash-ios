//
//  CategoryServices.swift
//  Categoryify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result
import ObjectMapper


protocol CategoryServices {
    func add(category:Category, username: String, complete:@escaping (_ result:Result<Category,NSError>)-> Void)
    func update(category:Category, username: String, complete:@escaping (_ result:Result<Category,NSError>)-> Void)
    func get(username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void)
    func delete(category:Category, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}

class CategoryServicesCenter: CategoryServices {
    func add(category:Category, username: String, complete:@escaping (_ result:Result<Category,NSError>)-> Void) {
        let json = ["category-id":category.id,
                    "description":category.description] as [String : Any]
        CategoryAPIProvider.request(CategoryAPI.add(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let Category:Category = try response.mapObject()
                    complete(Result.success(Category))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func update(category:Category, username: String, complete:@escaping (_ result:Result<Category,NSError>)-> Void) {
        let json = ["category-id":category.id,
                    "description":category.description] as [String : Any]
        CategoryAPIProvider.request(CategoryAPI.update(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let Category:Category = try response.mapObject()
                    complete(Result.success(Category))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func get(username: String, complete:@escaping (_ result:Result<[Category],NSError>)-> Void) {
        CategoryAPIProvider.request(CategoryAPI.get(username: username))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["categories"] as? [[String:Any]] {
                            let objects = Mapper<Category>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func delete(category:Category, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        let json = ["category-id":category.id,
                    "description":category.description] as [String : Any]
        CategoryAPIProvider.request(CategoryAPI.delete(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["status"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
