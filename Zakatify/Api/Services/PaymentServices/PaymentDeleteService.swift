//
//  PaymentDeleteService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
class PaymentDeleteService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var state: Bool?
        if let root = response as? [String: Any] {
            state = root.boolForKey(key: "state")
        }
        super.onFinish(state, error: error, completion: completion)
    }
}
