//
//  PaymentServices.swift
//  Paymentify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result
import ObjectMapper


protocol PaymentServices {
    func add(payment:Payment, username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void)
    func update(payment:Payment, username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void)
    func get(username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void)
    func delete(payment:Payment, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
    func setPrimary(payment:Payment, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}

class PaymentServicesCenter: PaymentServices {
    func add(payment:Payment, username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void) {
        let json: [String:Any] = ["payment-type":payment.paymentType.rawValue,
                                  "last-four":payment.lastFourDigit,
                                  "token":payment.token,
                                  "is-primary":payment.isPrimary]
        PaymentAPIProvider.request(PaymentAPI.add(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["paymentOptions"] as? [[String:Any]] {
                            let objects = Mapper<Payment>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func update(payment:Payment, username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void) {
        PaymentAPIProvider.request(PaymentAPI.update(username: username, json: payment.toJSON()))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["paymentOptions"] as? [[String:Any]] {
                            let objects = Mapper<Payment>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func get(username: String, complete:@escaping (_ result:Result<[Payment],NSError>)-> Void) {
        PaymentAPIProvider.request(PaymentAPI.get(username: username))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let category = json["paymentOptions"] as? [[String:Any]] {
                            let objects = Mapper<Payment>().mapArray(JSONArray: category)
                            complete(Result.success(objects))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func delete(payment:Payment, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        let json = ["pay-option-id":payment.id] as [String : Any]
        PaymentAPIProvider.request(PaymentAPI.delete(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["state"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    func setPrimary(payment:Payment, username: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        let json: [String:Any] = ["pay-option-id":payment.id]
        PaymentAPIProvider.request(PaymentAPI.setPrimary(username: username, json: json))
        { (result:Result<Response, MoyaError>) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    if let json = try filtedResponce.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                        if let status = json["state"] as? Bool {
                            complete(Result.success(status))
                        } else {
                            complete(Result.failure(response.defaultAPIError))
                        }
                    } else {
                        complete(Result.failure(response.defaultAPIError))
                    }
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
