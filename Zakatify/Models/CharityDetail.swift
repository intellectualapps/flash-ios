//
//  CharityDetail.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

protocol CharityDetail {
    var id: Int {get set}
    var newfeeds: [Feed] {get set}
    var name: String {get set}
    var description: String {get set}
    var slogan: String { get set }
    var logoUrl: String {get set}
    var rate: String {get set}
    var totalMoney: Int {get set}
    var donors: [Donor] {get set}
    var reviews: [Review] {get set}
    var tags: [Category] {get set}
    var added: Bool {get set}
    var ein: String {get set}
    var phoneNumber: String {get set}
    var reportUrl: String {get set}
    var address: String {get set}
    var trendDatas: [[String: Any]] {get set}
    var email: String {get set}
    var website: String {get set}
}

class Charity: CharityDetail, Mappable {
    enum JSONKey: String {
        case id = "charityId"
        case newFeeds = "newsFeed"
        case name = "charityName"
        case description = "description"
        case logoUrl = "charityLogo"
        case rate = "rating"
        case totalMoney = "totalDonation"
        case donors
        case numberOfDonators
        case reviews
        case tags = "categories"
        case added = "selected"
        case ein = "ein"
        case phoneNumber = "phoneNumber"
        case reportUrl = "reportUrl"
        case address = "address"
        case trendData = "trendData"
        case email
        case website = "websiteUrl"
        case slogan = "slogan"
    }
    var id: Int = 0
    var newfeeds: [Feed] = []
    var name: String = ""
    var description: String = ""
    var slogan = ""
    var logoUrl: String = ""
    var rate: String = ""
    var totalMoney: Int = 0
    var donors: [Donor] = []
    var numberOfDonators: Int = 0
    var reviews: [Review] = []
    var tags: [Category] = []
    var added: Bool = false
    var ein: String = ""
    var phoneNumber: String = ""
    var reportUrl: String = ""
    var address: String = ""
    var trendDatas: [[String: Any]] = [[String: Any]]()
    var email: String = ""
    var website: String = ""

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        newfeeds <- map[JSONKey.newFeeds.rawValue]
        name <- map[JSONKey.name.rawValue]
        description <- map[JSONKey.description.rawValue]
        logoUrl <- map[JSONKey.logoUrl.rawValue]
        rate <- map[JSONKey.rate.rawValue]
        totalMoney <- map[JSONKey.totalMoney.rawValue]
        donors <- map[JSONKey.donors.rawValue]
        numberOfDonators <- map[JSONKey.numberOfDonators.rawValue]
        reviews <- map[JSONKey.reviews.rawValue]
        tags <- map[JSONKey.tags.rawValue]
        added <- map[JSONKey.added.rawValue]
        ein <- map[JSONKey.ein.rawValue]
        phoneNumber <- map[JSONKey.phoneNumber.rawValue]
        reportUrl <- map[JSONKey.reportUrl.rawValue]
        address <- map[JSONKey.address.rawValue]
        address = (address as NSString).replacingOccurrences(of: ",", with: " ")
        trendDatas <- map[JSONKey.trendData.rawValue]
        email <- map[JSONKey.email.rawValue]
        website <- map[JSONKey.website.rawValue]
        slogan <- map[JSONKey.slogan.rawValue]
    }
    
}
