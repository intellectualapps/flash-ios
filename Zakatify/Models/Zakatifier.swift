//
//  Zakatifier.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

protocol Zakatifier {
    var verify: Bool {get set}
    var added: Bool {get set}
    var email: String {get set}
    var fullname: String {get}
    var firstName: String {get}
    var lastName: String {get}
    var username: String {get}
    var profileUrl: String {get set}
    var mobile: String {get set}
    var location: String {get set}

    //
    var goal: Float {get set}
    var current: Float {get set}
}

extension Zakatifier {
    var fullname: String {
        return firstName + " " + lastName
    }
    var nameAttributedString: NSMutableAttributedString {
        let att = NSMutableAttributedString(string: self.fullname + " ")
        if verify {
            let icAttributed = #imageLiteral(resourceName: "verified-green-small").toAttributedString()
            att.append(icAttributed)
        }
        return att
    }
    
    var avartarURL: URL? {
        if let url = URL(string: self.profileUrl) {
            return url
        }
        return nil
    }
    
    var progess: Float {
        if goal == 0 {
            return 0
        }
        if current >= goal {
            return 100
        }
        return  (current/goal)*100
    }
}

class ZakatifierInfo: Zakatifier, Mappable {
    enum JSONKey: String {
        case email = "email"
        case username
        case firstName
        case lastName
        case photoUrl = "photoUrl"
        case mobile = "phoneNumber"
        case location = "location"
        case verified
        case hash
        case creationDate

    }
    var verify: Bool = false
    var added: Bool = false
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var username: String = ""
    var profileUrl: String = ""
    var mobile: String = ""
    var location: String = ""
    var hash = ""
    var creationDate = ""

    var goal: Float = 0
    var current: Float = 0

    init() {
    }

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        print(map.JSON)
        email <- map[JSONKey.email.rawValue]
        username <- map[JSONKey.username.rawValue]
        firstName <- map[JSONKey.firstName.rawValue]
        lastName <- map[JSONKey.lastName.rawValue]
        profileUrl <- map[JSONKey.photoUrl.rawValue]
        mobile <- map[JSONKey.mobile.rawValue]
        location <- map[JSONKey.location.rawValue]
        verify <- map[JSONKey.verified.rawValue]
        hash <- map[JSONKey.hash.rawValue]
        creationDate <- map[JSONKey.creationDate.rawValue]
    }
}

class Donor: Zakatifier, Mappable {
    enum JSONKey: String {
        case username
        case firstName
        case lastName
        case verified
        case profileUrl = "photoUrl"
    }
    var verify: Bool = false
    var added: Bool = false
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var username: String = ""
    var profileUrl: String = ""
    var mobile: String = ""
    var location: String = ""
    var goal: Float = 0
    var current: Float = 0
    var rank: Int = 0
    var points: Int = 0

    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        username <- map[JSONKey.username.rawValue]
        firstName <- map[JSONKey.firstName.rawValue]
        lastName <- map[JSONKey.lastName.rawValue]
        profileUrl <- map[JSONKey.profileUrl.rawValue]
        verify <- map[JSONKey.verified.rawValue]
    }
}
