//
//  SideMenuModel.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class Count: Mappable {
    enum JSONKey: String {
        case count
    }
    var count: Int = 0
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count <- map[JSONKey.count.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.count.rawValue:count]
    }
}

class SideMenuModel: Mappable {
    enum JSONKey: String {
        case user = "userDetails"
        case donationProgress = "donationProgess"
        case userBoard
        case notification
        case message
        case counts
    }
    
    var user:UserInfo = UserInfo()
    var donationProgress: UserDonationProgess = UserDonationProgess()
    var userBoard: UserBoard = UserBoard()
    var notification: Count = Count()
    var message: Count = Count()
    var userCount = UserCount()
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user <- map[JSONKey.user.rawValue]
        donationProgress <- map[JSONKey.donationProgress.rawValue]
        userBoard <- map[JSONKey.userBoard.rawValue]
        notification <- map[JSONKey.notification.rawValue]
        message <- map[JSONKey.message.rawValue]
        userCount <- map[JSONKey.counts.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.user.rawValue:user.toJSON(),
                JSONKey.donationProgress.rawValue:donationProgress.toJSON(),
                JSONKey.userBoard.rawValue:userBoard.toJSON(),
                JSONKey.notification.rawValue:notification.toJSON(),
                JSONKey.message.rawValue:message.toJSON(),
                JSONKey.counts.rawValue: userCount.toJSON()]
    }
}

extension SideMenuModel {
    static let saveKey: String = "Saved-SideMenuModel"
    func save() {
        UserDefaults.standard.set(self.toJSON(), forKey: SideMenuModel.saveKey)
    }
    
    class func load() -> SideMenuModel? {
        guard let savedJson = UserDefaults.standard.object(forKey: saveKey) as? [String : Any] else {
            return nil
        }
        if let object = Mapper<SideMenuModel>().map(JSON: savedJson) {
            return object
        }
        return nil
    }
}
