//
//  Donation.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol Donation {
    var charityName: String {get set}
    var money: Int {get set}
    var total: Int {get set}
}

