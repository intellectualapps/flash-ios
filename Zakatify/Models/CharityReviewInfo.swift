//
//  CharityReviewInfo.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CharityReviewInfo {
    var charityReviews: [CharityReview] = []
    var pageNumber = 0
    var pageSize = 0
    var totalCount = 0
    
    init(dict: [String: Any]) {
        if let _charities = dict["reviews"] as? [[String: Any]] {
            for charity in _charities {
                if let _charity = CharityReview(JSON: charity) {
                    charityReviews.append(_charity)
                }
            }
        }
        pageNumber = dict.intForKey(key: "pageNumber")
        pageSize = dict.intForKey(key: "pageSize")
        totalCount = dict.intForKey(key: "totalCount")
    }
}
