//
//  SectionInfo.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

class SectionInfo: NSObject {
    var identifier = ""
    var title = ""
    var content: AnyObject?
    var height: CGFloat = 0
    var sectionId = 0
    var cells = [CellInfo]()

    convenience init(indentify: String, title: String, height: CGFloat, index: Int?, contentObj: AnyObject? = nil, constructor: () -> ([CellInfo])) {
        self.init()
        self.identifier = indentify
        self.title = title
        self.height = height
        self.cells = constructor()
        self.sectionId = index ?? 0
        self.content = contentObj
    }
}
