//
//  UnreadManager.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 10/13/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import Firebase

class UnreadManager: NSObject {
    static let shared = UnreadManager()
    var rooms = [String]()
    var observable: Variable<String>?
    

    func startListenForRoom(room: String, observable: Variable<Int>) {
        if !rooms.contains(room) {
           rooms.append(room) 
        }
    }
}
