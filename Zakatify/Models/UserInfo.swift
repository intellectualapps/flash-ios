//
//  UserInfo.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper
import FSwiftValidator

protocol User: Mappable {
    var username: String {get set}
    var email: String {get set}
}

class UserInfo: User, Mappable, Zakatifier {

    static let userValidator = RequiredRule(message: "Please input @username")
    static let emailValidator = EmailRule(message: "Invalid email")
    static let firstNameValidator = RequiredRule(message: "Please input first name")
    static let lastNameValidator = RequiredRule(message: "Please input last name")
    static let mobileValidator_required = RequiredRule(message: "Please input phone number")
    static let mobileValidator_format = PhoneRule(message: "Invalid moblie number")
    static let locationValidator = RequiredRule(message: "Please input Location")
    
    enum JSONKey:String {
        case username
        case firstName
        case lastName
        case email
        case creationDate
        case authToken
        case facebookEmail
        case twitterEmail
        case photoUrl
        case location
        case phoneNumber
        case tempKey
        case verified = "verified"
        case donations
        case followers
        case portfolios
        case badges
    }
    
    var username = ""
    var password = ""
    var firstName: String = ""
    var lastName: String = ""
    var mobile: String = ""
    var location: String = ""
    var email: String = ""
    var authToken: String?
    var tempKey: String?
    var facebookCompleteName: String = ""
    var facebookEmail: String = ""
    var twitterUsername: String = ""
    var twitterName: String = ""
    var twitterEmail: String = ""
    var profileUrl: String = ""
    var tags: [Tag] = []
    
    var photoUrlLink: URL? {
        if profileUrl.isEmpty == true {
            return nil
        }
        if let url = URL(string: profileUrl) {
            return url
        }
        return nil
    }
    
    // Zakatifier
    var verify: Bool = false
    var added: Bool = false
    var fullname: String {
        return self.firstName + " " + self.lastName
    }
    var goal: Float = 0
    var current: Float = 0
    var rank: Int = 0
    var points: Int = 0
    var donations: Int = 0
    var followers: Int = 0
    var portfolios = 0
    var badges = 0
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        username <- map[JSONKey.username.rawValue]
        firstName <- map[JSONKey.firstName.rawValue]
        lastName <- map[JSONKey.lastName.rawValue]
        email <- map[JSONKey.email.rawValue]
        mobile <- map[JSONKey.phoneNumber.rawValue]
        location <- map[JSONKey.location.rawValue]
        authToken <- map[JSONKey.authToken.rawValue]
        tempKey <- map[JSONKey.tempKey.rawValue]
        facebookEmail <- map[JSONKey.facebookEmail.rawValue]
        twitterEmail <- map[JSONKey.twitterEmail.rawValue]
        profileUrl <- map[JSONKey.photoUrl.rawValue]
        verify <- map[JSONKey.verified.rawValue]
        donations <- map[JSONKey.donations.rawValue]
        followers <- map[JSONKey.followers.rawValue]
        portfolios <- map[JSONKey.portfolios.rawValue]
        badges <- map[JSONKey.badges.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        var json = [JSONKey.username.rawValue:username,
                    JSONKey.email.rawValue:email,
                    JSONKey.firstName.rawValue:firstName,
                    JSONKey.lastName.rawValue:lastName,
                    JSONKey.location.rawValue:location,
                    JSONKey.phoneNumber.rawValue:mobile,
                    JSONKey.photoUrl.rawValue:profileUrl,
                    JSONKey.facebookEmail.rawValue:facebookEmail,
                    JSONKey.twitterEmail.rawValue:twitterEmail,
                    JSONKey.verified.rawValue: verify,
                    JSONKey.donations.rawValue: donations,
                    JSONKey.followers.rawValue: followers,
                    JSONKey.portfolios.rawValue: portfolios,
                    JSONKey.badges.rawValue: badges] as [String : Any]
        if let token = authToken {
            json[JSONKey.authToken.rawValue] = token
        }
        if let temkey = tempKey {
            json[JSONKey.tempKey.rawValue] = temkey
        }
        return json
    }
    
    func toSaveJSON() -> [String : Any] {
        var json = [JSONKey.username.rawValue:username,
                    JSONKey.email.rawValue:email,
                    JSONKey.firstName.rawValue:firstName,
                    JSONKey.lastName.rawValue:lastName,
                    JSONKey.location.rawValue:location,
                    JSONKey.phoneNumber.rawValue:mobile,
                    JSONKey.photoUrl.rawValue:profileUrl,
                    JSONKey.facebookEmail.rawValue:facebookEmail,
                    JSONKey.twitterEmail.rawValue:twitterEmail,
                    JSONKey.verified.rawValue: verify,
                    JSONKey.donations.rawValue: donations,
                    JSONKey.followers.rawValue: followers,
                    JSONKey.portfolios.rawValue: portfolios,
                    JSONKey.badges.rawValue: badges] as [String : Any]
        if let token = authToken {
            json[JSONKey.authToken.rawValue] = token
        }
        if let temkey = tempKey {
            json[JSONKey.tempKey.rawValue] = temkey
        }
        return json
    }
    

    //Invoke this to sync current login user to UserDefault or database
    func sync() {
        //Now sync to user default
    }
}

