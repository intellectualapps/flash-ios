//
//  UINavigationController+Extension.swift
//  Zakatify
//
//  Created by Thang Truong on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
// MARK: - contentViewController

extension UIViewController {
    var contentViewController: UIViewController {
        if let navContentVC = self as? UINavigationController {
            return navContentVC.visibleViewController!
        } else {
            return self
        }
    }

    class func logEvent(eventName: LogEventName, parameters: AppEvent.ParametersDictionary? = nil) {
        if let parameters = parameters {
            AppEventsLogger.log(eventName.rawValue, parameters: parameters, valueToSum: nil, accessToken: nil)
        } else {
            AppEventsLogger.log(eventName.rawValue)
        }
    }
}

enum LogEventName: String {
    case signupPreferences = "Singup_Preferences"
    case signupCalculator = "Signup_Calculator"
    case singupCalculatorBack = "Singup_calculator_back"
    case signupCalulate = "Signup_calulate"
    case singupDeductionTab = "Singup_deduction_tab"
    case signupPaypal = "Signup_paypal"
    case signupIntrest = "Signup_Intrest"
    case signupReady = "Signup_Ready"
    case openProfile = "Open_profile"
    case follow = "Follow"
    case search = "Search"
    case zakatifiersTab = "Zakatifiers_tab"
    case preferencesPreferences = "Preferences_Preferences"
    case preferencesCalculator = "Preferences_Calculator"
    case preferencesSave = "Preferences_save"

}
