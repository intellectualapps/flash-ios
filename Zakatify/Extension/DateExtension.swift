//
//  DateExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

let default_date_format = "yyyy/MM/dd"
let default_date_format_forHeader = "yyyy/MM/dd HH:mm"

extension Date {
    static func date(fromString: String, format: String) -> Date? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = format
        return dateFormate.date(from: fromString)
    }
    
    static func UTCToLocal(date: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let string = dateFormatter.string(from: dt!)
        return dateFormatter.date(from: string)
    }

    func timeAgoDisplay() -> String {

        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
        let monthsAgo = calendar.date(byAdding: .month, value: -1, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff)s"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff)m"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff)h"
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff)d"
        } else if monthsAgo < self {
            let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
            return "\(diff)w"
        }

        let diff = Calendar.current.dateComponents([.month], from: self, to: Date()).month ?? 0
        return "\(diff)mth"

    }

    func toTimeStampMillis() -> Int64{
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    func stringWithUnixTime() -> String {
        var resultString = ""
        var delta = abs(Date().timeIntervalSince1970 - self.timeIntervalSince1970)
        if delta < 60 {
            delta = 60
        }
        let oneHour = Double(60 * 60)
        let oneweek =  7 * 24 * oneHour
        if delta < oneHour {
            resultString = String(Int(delta / 60)) + "m ago"
        } else if delta < 24 * oneHour {
            resultString = String(Int(delta / 3600)) + "h ago"
        } else if  delta < 24 * 2 * oneHour {
            resultString =  "yesterday".localized()
        } else if  delta < oneweek {
            resultString =  String(Int(delta / 86400)) + "d ago"
        } else if delta == oneweek {
            resultString =  "1w ago"
        } else {
            resultString =  self.dateStringWithFormat(default_date_format)
        }
        return resultString
    }
    
    func dateStringWithFormat(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle =  DateFormatter.Style.full
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    /**
     - Nếu trong ngày, hiển thị thời gian AM/PM
        9:03 AM, 11:20 PM
     - Nếu cách đó 1 tuần, hiển thị ngày và thời gian AM/PM
        Thu 9:10 PM
     - Trên 1 tuần, hiển thị ngày, tháng, năm và thời gian AM/PM
        Aug 10, 2017 10:48 AM
     */
    func timeAgo() -> String {
        var dateString = ""
        if self.isToday {
            dateString = self.toShortTime()
        } else if Date().prevWeek(at: .auto) < self &&
            Date() > self {
            dateString = self.toShortTime2()
        } else {
            dateString = self.toDate()
        }
        return dateString
    }
}

extension Int64 {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self-1))
    }
}
