//
//  UIImageExtentions.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

extension UIImage {
    func toAttributedString() -> NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = self
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        return attachmentString
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
