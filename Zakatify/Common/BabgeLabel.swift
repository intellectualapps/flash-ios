//
//  BabgeLabel.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class BabgeLabel: UILabel {

    override var text: String? {
        didSet {
            layout()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    private func layout() {
        guard let text = text else {
            self.isHidden = true
            return
        }
        guard let value = Int(text) else {
            self.isHidden = true
            return
        }
        if value == 0 {
            self.isHidden = true
            return
        }
        let center = self.center
        self.isHidden = false
        self.sizeToFit()
        var frame = self.frame
        frame.size.height += 0.3 * self.font.pointSize
        frame.size.width = self.frame.size.width < self.frame.size.height ? frame.size.height : frame.size.width + self.font.pointSize
        self.frame = frame
        self.center = center
        self.layer.cornerRadius = frame.size.height / 2.0
        self.layer.masksToBounds = true
        
        self.clipsToBounds = true
    }

}
