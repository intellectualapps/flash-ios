//
//  MaterialTextFeild.swift
//  TextFeild
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 framgia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MaterialTextFeild: UITextField {
    var disposeBag = DisposeBag()
    private var placeholderLable: UILabel!
    private var normalFrame: CGRect = CGRect.zero
    @IBInspectable var seeUnderLine: Bool = false
    private var underline: UIView!

    override var text: String? {
        set {
            super.text = newValue
            layout()
        }
        get {
            return super.text
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initLabel()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        initLabel()
    }
    
    private func initLabel() {
        if placeholderLable != nil {
            return
        }
        
//        self.contentVerticalAlignment = .bottom
        self.borderStyle = .none
        if let lable = self.subviews.filter({ (view) -> Bool in
            return view is UILabel
        }).first  as? UILabel {
            placeholderLable = UILabel(frame: lable.frame)
            placeholderLable.font = lable.font
            placeholderLable.text = placeholder
            placeholderLable.textColor = UIColor.lightGray
            placeholderLable.textAlignment = lable.textAlignment
            normalFrame = lable.frame
            self.addSubview(placeholderLable)
            placeholderLable.isHidden = true
            layout()
            
            self.rx.text.bind { [unowned self] (text) in
                self.layout()
                }.addDisposableTo(disposeBag)
        }
        if seeUnderLine {
            underline = UIView(frame: CGRect(x: 0, y: frame.size.height - 1, width: self.frame.size.width, height: 1))
            underline.backgroundColor = UIColor.lightGray
            self.addSubview(underline)
            
            self.rx.controlEvent(UIControlEvents.editingDidBegin).bind {
                self.underline?.backgroundColor = self.tintColor
                }.addDisposableTo(disposeBag)
            
            self.rx.controlEvent(UIControlEvents.editingDidEnd).bind {
                self.underline?.backgroundColor = UIColor.lightGray
                }.addDisposableTo(disposeBag)
        }
    }
    
    private func layout() {
        guard let text = super.text else {
            layoutWhenHaveNoContent()
            return
        }
        if text.isEmpty {
            layoutWhenHaveNoContent()
            return
        }
        layoutWhenHaveCotent()
    }
    
    private func layoutWhenHaveCotent() {
        guard let placeholderLable = placeholderLable else {
            return
        }
        if placeholderLable.isHidden == false {
            return
        }
        placeholderLable.isHidden = false
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) {
                placeholderLable.font = UIFont.systemFont(ofSize: 10)
                placeholderLable.frame.origin.y = 0
                placeholderLable.sizeToFit()
            }
        }
    }
    
    private func layoutWhenHaveNoContent() {
        guard let placeholderLable = placeholderLable else {
            return
        }
        if placeholderLable.isHidden == true {
            return
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                placeholderLable.font = self.font
                placeholderLable.frame = self.normalFrame
            }) { (success) in
                placeholderLable.isHidden = true
            }
        }
    }
}
