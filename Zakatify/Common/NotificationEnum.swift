//
//  NotificationEnum.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

enum NotificationName: String {
    case NetworkStateChange = "KNotificationNetworkStateDidChange"
    case LoginStateDidChange = "KNotificationLoginStateDidChange"

    public func name() -> NSNotification.Name {
        return Notification.Name(rawValue: self.rawValue)
    }
}
