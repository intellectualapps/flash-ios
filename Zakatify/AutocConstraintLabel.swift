//
//  AutoSizeLabel.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 5/2/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import DhtLib

class AutocConstraintLabel: UILabel {
    override var text: String? {
        didSet {
            self.setNeedsUpdateConstraints()
            if let widthConstraint = self.constraintFor(attribute: NSLayoutAttribute.width) {
                widthConstraint.constant = self.totalWidth()
            }
        }
    }

    func totalWidth() -> CGFloat {
        let txt = text ?? ""
        let font  = self.font ?? UIFont.systemFont(ofSize: 16)
        let size = txt.sizeWithBoundSize(boundSize: ScreenSize(), option: .usesLineFragmentOrigin, font: font)
        return size.width + 15
    }
}
