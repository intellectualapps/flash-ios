//
//  InvitePresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/13/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Contacts

protocol InviteViewPresenter: TableViewPresenter {
    var view:InvitePresenterView? {get}
    init(view: InvitePresenterView)
    func invitation(index: Int) -> Invitation?
    func invite(email: String)
}

protocol InvitePresenterView: TableView {
    
}

class InvitePresenter: InviteViewPresenter {
    
    weak var view: InvitePresenterView?
    
    var serverInvitations: [Invitation] = [] {
        didSet {
            requestGrant()
        }
    }
    var invitations: [Invitation] = []
    var userContactEmails: [String] = [] {
        didSet{
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.view?.refreshing()
            })
            invitations = serverInvitations
            var newInvitations: [Invitation] = []
            for email in userContactEmails {
                if let _ = serverInvitations.filter({ (invitation) -> Bool in
                    return invitation.invitee == email
                }).first {
                    continue
                }
                let invitation = Invitation()
                invitation.invitee = email
                invitation.invitor = username
                newInvitations.append(invitation)
            }
            invitations += newInvitations
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.view?.refreshed()
            })
        }
    }
    
    var services: InviteServices = InviteServicesCenter()
    var pageCount: Page = PageCount()
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    required init(view: InvitePresenterView) {
        self.view = view
    }
    
    func invitation(index: Int) -> Invitation? {
        if index >= 0, index < invitations.count {
            return invitations[index]
        }
        return nil
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return invitations.count
    }
    
    func invite(email: String) {
        view?.showLoading()
        services.sendInvitation(username: username, inviteeEmail: email) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view?.hideLoading()
            switch result {
            case .success(let state):
                strongSelf.view?.refreshed()
                strongSelf.refresh()
            case .failure(let error):
                strongSelf.view?.showAlert(error.localizedDescription)
            }
            strongSelf.view?.refreshed()
        }
    }
    
    func refresh() {
        view?.refreshing()
        services.userInvitations(username: username, page: 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view?.hideLoading()
            switch result {
            case .success((invitations:let invitations, page: let page,size: let size, total: let total)):
                strongSelf.serverInvitations = invitations
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                strongSelf.view?.showAlert(error.localizedDescription)
            }
            strongSelf.view?.refreshed()
        }
    }
    
    func refresh(search: String?) {
        
    }
    
    func loadmore() {
        if shouldLoadmore() == false {
            return
        }
        view?.refreshing()
        services.userInvitations(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view?.hideLoading()
            switch result {
            case .success((invitations:let invitations, page: let page,size: let size, total: let total)):
                strongSelf.serverInvitations += invitations
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                strongSelf.view?.showAlert(error.localizedDescription)
            }
            strongSelf.view?.refreshed()
        }
    }
    
    func shouldLoadmore() -> Bool {
        return invitations.count > 0 && invitations.count < pageCount.total
    }
    
    private func requestGrant() {
        let status = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        if status == .denied {
            
        }
        
        let store = CNContactStore()
        store.requestAccess(for: CNEntityType.contacts) { [weak self] (granted, error) in
            if !granted {
                return
            }
            
            self?.getEmails()
        }
    }
    
    private func getEmails() {
        userContactEmails = []
        let store = CNContactStore()
        let reqest = CNContactFetchRequest(keysToFetch: [CNContactEmailAddressesKey as CNKeyDescriptor])
        do {
            try store.enumerateContacts(with: reqest, usingBlock: { [weak self] (contact, stop) in
                let emails = contact.emailAddresses
                for email in emails {
                    self?.userContactEmails.append(email.value as String)
                }
            })
        } catch {
            
        }
    }
    
}
