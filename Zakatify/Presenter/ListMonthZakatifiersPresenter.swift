//
//  ListMonthZakatifiersPresenter.swift
//  Zakatify
//
//  Created by center12 on 5/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ListMonthZakatifiersPresenter: ListZakatifiersPresenter {
    override func refresh() {
        view.refreshing()
        services.topMonthZakatifiers(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((zakatifiers:let zakatifiers, page:let page ,size:let size, total:let total)):
                strongSelf.zakatifiers = zakatifiers
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.zakatifiers = []
            }
            strongSelf.view.refreshed()
        }
    }
    
    override func refresh(search: String?) {
        view.refreshing()
        services.topMonthZakatifiers(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((zakatifiers:let zakatifiers, page:let page ,size:let size, total:let total)):
                strongSelf.zakatifiers = zakatifiers
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                if let search = search, search.count > 0 {
                    let params: AppEvent.ParametersDictionary = [
                        AppEventParameterName.custom("Search term"): search,
                        AppEventParameterName.custom("Zakitifiers_Result_number"): String(zakatifiers.count),
                        AppEventParameterName.custom("Total_Results"): String(zakatifiers.count)
                    ]
                    UIViewController.logEvent(eventName: .search, parameters: params)
                }
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.zakatifiers = []
            }
            strongSelf.view.refreshed()
        }
    }
}
