//
//  CharityPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Moya
import RxSwift

protocol CharityDetailView: TableView {
    func userDidAddToPorfolio()
    func userDidDonate()
    func userDidWriteReview()
    func dataChanged()
}

protocol CharityDetailViewPresenter {
    var view: CharityDetailView {get set}
    var charity: CharityDetail {get set}
    var reviews: Variable<[CharityReview]> {get set}
    init(view:CharityDetailView, model:CharityDetail)
    func donate(money:Int, payment: Payment, completion:@escaping ()->())
    func addToPortfolio()
    func removeFromPortfolio()
    func review(rate: Int, comment: String)
    func refresh()
    func loadmore()
    func shouldLoadmore() -> Bool
    var charityNewFeed: NSAttributedString {get}
    var charityLogoUrl: String {get}
    var charityName: String {get}
    var charityRate: Int {get}
    var charityDesription: String {get}
    var charitySlogan: String { get }
    var donateDescription: String {get}
    var moneyDescription: String {get}
    var donorsAvatarUrls:[String] {get}
    var tagDescription: String {get}
    var userPortfolio: Bool {get}
    var charityEmail: String {get}
    var charityWebsite: URL? {get}
    var isRefresh: Bool { get set }
}

extension CharityDetailViewPresenter {
    var newFeedIcon: NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "donated-green")
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        return attachmentString
    }
    var charityNewFeed: NSAttributedString {
        if charity.newfeeds.count == 1,
            charity.newfeeds.first?.activityBy == "",
            charity.newfeeds.first?.activityType == "" {
            return NSAttributedString(string: "")
        }
        if charity.newfeeds.count == 0 {
            return NSAttributedString(string: "")
        }
        let att = NSMutableAttributedString(attributedString: newFeedIcon)
        att.append(NSAttributedString(string: " "))
        att.append(NSMutableAttributedString(string: charity.newfeeds.first?.description ?? ""))
        return att
    }
    var charityLogoUrl: String {
        return charity.logoUrl
    }
    var charityName: String {
        return charity.name
    }
    var charityRate: Int {
        if let rateDouble = Double(charity.rate) {
            return Int(rateDouble)
        }
        return 0
    }
    var charityDesription: String {
        return charity.description
    }
    
    var charitySlogan: String {
        return charity.slogan
    }
    
    var donateDescription: String {
        if charity.donors.count == 0 {
            return "No donations yet \nBe the first to donate"
        }
        return "\(charity.donors.count) donated $\(charity.totalMoney)"
//         let moreDonors = charity.donors.count - donorsAvatarUrls.count
//         if charity.donors.count == 0 {
//             return "Be the first to donate"
//         }
//         if moreDonors == 0 {
//             return "donated"
//         } else {
//             return "+\(charity.donors.count) donated"            
//         }
    }
    var moneyDescription: String {
        return "$\(charity.totalMoney)"
    }
    var donorsAvatarUrls:[String] {
        var urls = [String]()
        for donor in charity.donors {
            urls.append(donor.profileUrl)
        }
        return urls
    }
    var tagDescription: String {
        return charity.tags.reduce("", { (result, tag) -> String in
            return "\(result)  #\(tag.description)"
        })
    }
    
    var userPortfolio: Bool {
        return charity.added
    }
    
    var charityEmail: String {
        return charity.email
    }
    
    var charityWebsite: URL? {
        return URL(string: charity.website)
    }
}

class CharityDetailPresenter: CharityDetailViewPresenter {
    
    enum Notification: String {
        case added
        case remove
        case review
    }

    unowned var view: CharityDetailView
    var charityViewModel = CharityViewModel()
    var charity: CharityDetail
    var reviews: Variable<[CharityReview]> = Variable([CharityReview]())
    var pageCount: Page = PageCount()
    let service: CharityServices = CharityServicesCenter()
    var canRequest = true
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    required init(view: CharityDetailView, model: CharityDetail) {
        self.view = view
        self.charity = model
        NotificationCenter.default.addObserver(self, selector: #selector(getReviews), name: NotificationKey.review, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func donate(money: Int, payment: Payment, completion: @escaping ()->()) {
        if money <= 0 {
            view.showAlert("please input number more than zero")
        } else {
            view.showLoading()
            service.donate(id: charity.id, amount: money) { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.view.hideLoading()
                switch result {
                case .success:
                    strongSelf.view.showAlert("Your donation has been successfully made")
                    completion()
                case .failure(let error):
                    strongSelf.view.showAlert(error.localizedDescription)
                }
            }
        }
    }
    
    func addToPortfolio() {
        view.showLoading()
        service.addToPortfolio(username: username, charity: self.charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                strongSelf.charity.added = true
                NotificationCenter.default.post(name: NSNotification.Name(Notification.added.rawValue), object: nil)
                strongSelf.view.userDidAddToPorfolio()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func removeFromPortfolio() {
        view.showLoading()
        service.removeFromPortfolio(username: username, charity: self.charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                strongSelf.charity.added = false
                NotificationCenter.default.post(name: NSNotification.Name(Notification.remove.rawValue), object: nil)
                strongSelf.view.userDidAddToPorfolio()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func review(rate: Int, comment: String) {
        view.showLoading()
        service.review(id: charity.id, rate: rate, comment: comment, username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let _):
                strongSelf.view.userDidWriteReview()
                NotificationCenter.default.post(name: NotificationKey.review, object: nil)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }

    @objc func getReviews() {
        view.showLoading()
        canRequest = false
        charityViewModel.getCharitiesReview(id: charity.id, page: pageCount.page, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.canRequest = true
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let CharityReviewInfo = response as? CharityReviewInfo {
                    strongSelf.reviews.value = CharityReviewInfo.charityReviews
                    strongSelf.pageCount = PageCount(page: CharityReviewInfo.pageNumber, size: CharityReviewInfo.pageSize, total: CharityReviewInfo.totalCount)
                    strongSelf.view.dataChanged()
                }
            }
        }
        
        /*
        requestReviews = service.charityReviews(id: charity.id, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            strongSelf.requestReviews = nil
            switch result {
            case .success((reviews:let reviews, page:let page ,size:let size, total:let total)):
                strongSelf.reviews = reviews
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }*/
    }
    
    var isRefresh: Bool = false
    @objc func refresh() {
        if isRefresh {return}
        isRefresh = true
        view.showLoading()
        charityViewModel.getCharityDetail(id: charity.id, username: username) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                strongSelf.getReviews()
                if let charity = response as? Charity {
                    strongSelf.charity = charity
                    strongSelf.view.dataChanged()
                }
                strongSelf.view.loadedMore()
            }
        }
        
        /*service.getDetails(id: charity.id, username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.isRefresh = false
            strongSelf.view.hideLoading()
            strongSelf.getReviews()
            switch result {
            case .success(let charityDetail):
                strongSelf.charity = charityDetail
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }*/
    }

    var requestReviews: Cancellable?
    func loadmore() {
//        if shouldLoadmore() == false || requestReviews != nil {
//            return
//        }
        if !canRequest {
            return
        }
        canRequest = false
        view.loadingMore()
        charityViewModel.getCharitiesReview(id: charity.id, page: pageCount.page + 1, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.canRequest = true
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                var tmp = strongSelf.reviews.value
                if let CharityReviewInfo = response as? CharityReviewInfo {
                    tmp.append(contentsOf: CharityReviewInfo.charityReviews)
                    strongSelf.pageCount = PageCount(page: CharityReviewInfo.pageNumber, size: CharityReviewInfo.pageSize, total: CharityReviewInfo.totalCount)
                }
                strongSelf.reviews.value = tmp
                strongSelf.view.dataChanged()
                strongSelf.view.loadedMore()
            }
        }
               
        /*requestReviews = service.charityReviews(id: charity.id, page: pageCount.page + 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            strongSelf.requestReviews = nil
            switch result {
            case .success((reviews:let reviews, page:let page ,size:let size, total:let total)):
                strongSelf.reviews += reviews
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.loadedMore()
        }*/
    }
    func shouldLoadmore() -> Bool {
        return reviews.value.count < pageCount.total && reviews.value.count > 0
    }
}
