//
//  MessagePresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 10/6/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Firebase

protocol MessagePresenterView: CommonView {
    func didSentMessage()
    func didReceivedNewMessage()
}
class MessagePresenter {
    weak var view: MessagePresenterView?
    var user: Zakatifier
    var otherUser: Zakatifier
    

    var roomID: String? {
        didSet {
            guard let roomID = roomID else {
                return
            }
            setOnline(status: true)
            getMessages()
            observePartnersPresence()
        }
    }

    var queryLimit = 100
    var messages: [MessageInfo] = []
    var service: ChatServicesCenter = ChatServicesCenter()
    init(view: MessagePresenterView?, user: Zakatifier, otherUser: Zakatifier, queryLimit: Int = 100) {
        self.view = view
        self.user = user
        self.otherUser = otherUser
        self.queryLimit = queryLimit
        NotificationCenter.default.addObserver(self, selector: #selector(forceground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(background), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }

    @objc private func background() {
        setOnline(status: false)
    }
    
    @objc private func forceground() {
        setOnline(status: true)
    }
    
    func initRoom() {
        view?.showLoading()
        service.getRoomID(username: user.username, otherUsername: otherUser.username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view?.hideLoading()
            switch result {
            case .success(let room) :
                strongSelf.roomID = room.id
            case .failure(let error):
                strongSelf.view?.showAlert(error.localizedDescription)
            }
        }
    }

    deinit {
        setOnline(status: false)
        NotificationCenter.default.removeObserver(self)
        FirebaseConstants.refs.databaseConnections.removeObserver(withHandle: messageHandle)
        FirebaseConstants.refs.databaseConnections.removeObserver(withHandle: partnersHandle)
    }

    var messageHandle: UInt = 0
    func getMessages() {
        guard let roomID = roomID else {
            return
        }
        print("room id = \(roomID)")
        messageHandle = FirebaseConstants.refs.databaseConnections.child(roomID).child("messages").queryLimited(toLast: UInt(queryLimit)).observe(.childAdded, with: {[weak self] (snapshot) in
            guard let strongSelf = self else {
                return
            }
            guard let mgsDict = snapshot.value as? [String: Any],
                let username = mgsDict["author"] as? String,
                let text = mgsDict["text"] as? String,
                let date = mgsDict["time"] as? Int64,
                let id = mgsDict["id"] as? String
                else {
                    return
                }
            var senderId = strongSelf.user.username
            var senderDisplayName = strongSelf.user.fullname
            if username == strongSelf.otherUser.username {
                senderId = strongSelf.otherUser.username
                senderDisplayName = strongSelf.otherUser.fullname
            }
            let millis = date/1000
            let msg = MessageInfo(senderId: senderId, senderDisplayName: senderDisplayName, date: millis.toDate(), text: text)
            msg.id = id
            msg.author = username
            strongSelf.messages.append(msg)
            strongSelf.view?.didReceivedNewMessage()
        })
    }

    var partnersOnlineStatus: Bool = false
    var partnersHandle: UInt = 0
    private func observePartnersPresence() {
        guard let roomID = roomID else {
            return
        }
        let _username = otherUser.username.replacingOccurrences(of: ".", with: "%")
        if _username.isEmpty {
            return
        }
        partnersHandle = FirebaseConstants.refs.databaseConnections.child(roomID).child("participants").child(_username).observe(.value, with: { [weak self] (snapshot) in
            if let val = snapshot.value as? Bool {
                self?.partnersOnlineStatus = val
            }
        })
    }

    func send(message: String) {
        guard let roomID = roomID else {
            return
        }
        let ref = FirebaseConstants.refs.databaseConnections.child(roomID).child("messages").childByAutoId()
        let newValue: [String: Any] = [ "read": 0,
                                        "id": ref.key,
                                        "author": user.username,
                                        "text": message,
                                        "time": Date().toTimeStampMillis()
                                       ]

        ref.setValue(newValue) { [weak self] (err, ref) in
            guard let strongSelf = self else {
                return
            }
            if let err = err {
                strongSelf.view?.showAlert(err.localizedDescription)
            } else {
                strongSelf.view?.didSentMessage()
                self?.sendPush()
            }
        }
    }

    public func updateRead(msgid: String) {
        guard let roomid = self.roomID else {
            return
        }
        let child = FirebaseConstants.refs.databaseConnections.child(roomid).child("messages").child(msgid).child("read")
        child.setValue(1)
    }

    private func sendPush() {
        guard let roomId = roomID , partnersOnlineStatus == false else { return }
        service.push(username: user.username, receiver: otherUser.username, connectionID: roomId) { (result) in
            switch result {
            case .success(_):
                break
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func setOnline(status: Bool) {
        guard let roomID = roomID else {
            return
        }
        let _username = user.username.replacingOccurrences(of: ".", with: "%")
        if _username.isEmpty {
            return
        }
        FirebaseConstants.refs.databaseConnections.child(roomID).child("participants").child(_username).setValue(status)
    }
}


