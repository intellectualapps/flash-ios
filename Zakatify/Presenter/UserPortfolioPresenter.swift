//
//  UserPortfolioPresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation


class UserPortfolioPresenter: CharityListPresenter {
    
    
    override func refresh() {
        view.refreshing()
        service.userPortfolio(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                for charity in charities {
                    charity.added = true
                }
                strongSelf.charities.value = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
    override func loadmore() {
        if shouldLoadmore() == false {
            return
        }
        view.refreshing()
        service.userPortfolio(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                for charity in charities {
                    charity.added = true
                }
                var tmp = strongSelf.charities.value
                if charities.count > 0 , let _charities = charities as? [CharityDetail] {
                    tmp.append(contentsOf: _charities)
                    strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                }
                strongSelf.charities.value = tmp

            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
}
