//
//  MessageListPresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseConstants {
    struct refs {
        static let databaseRoot = Database.database().reference()
        static let databaseConnections = databaseRoot.child("connections")
    }
}
protocol MessageListPresenterView: TableView {

}
class MessageListPresenter: TableViewPresenter {

    var view: MessageListPresenterView?
    var list: [Room] = [] {
        didSet{
            for room in list {
                room.user = self.user
            }
        }
    }
    var service = ChatServicesCenter()
    var user: UserInfo
    init(view: MessageListPresenterView?, user: UserInfo) {
        self.view = view
        self.user = user
    }

    func numberOfRowsInSection(section: Int) -> Int {
        return list.count
    }

    func roomAt(index: IndexPath) -> Room? {
        if index.row < list.count {
            return list[index.row]
        }
        return nil
    }

    func refresh() {
        view?.refreshing()
        service.getRooms(username: user.username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let rooms):
                strongSelf.list = rooms
                
                for i in 0 ..< rooms.count {
                    self?.getMessages(roomID: rooms[i].id, completion: { (msg) in
                        rooms[i].lastDate = msg.date
                        rooms[i].lastMessage = msg.text
                        strongSelf.list.sort { (room1, room2) -> Bool in
                            let date1 = room1.lastDate?.timeIntervalSince1970 ?? 0
                            let date2 = room2.lastDate?.timeIntervalSince1970 ?? 0
                            return date1 > date2
                        }
                        strongSelf.view?.refreshed()
                    })
                }
            case .failure(let error):
                strongSelf.view?.showAlert(error.localizedDescription)
            }
            strongSelf.view?.refreshed()
        }
    }

    func refresh(search: String?) {

    }

    func loadmore() {

    }

    func shouldLoadmore() -> Bool {
        return false
    }
    
    var messageHandle: UInt = 0
    var queryLimit = 1
    func getMessages(roomID: String, completion: @escaping ((MessageInfo)->())) {
        print("room id = \(roomID)")
        messageHandle = FirebaseConstants.refs.databaseConnections.child(roomID).child("messages").queryLimited(toLast: UInt(queryLimit)).observe(.childAdded, with: {[weak self] (snapshot) in
            guard let strongSelf = self else {
                return
            }
            guard let mgsDict = snapshot.value as? [String: Any],
                let username = mgsDict["author"] as? String,
                let text = mgsDict["text"] as? String,
                let date = mgsDict["time"] as? Int64,
                let id = mgsDict["id"] as? String
                else {
                    return
            }
            var senderId = strongSelf.user.username
            var senderDisplayName = strongSelf.user.fullname
            let millis = date/1000
            let msg = MessageInfo(senderId: senderId, senderDisplayName: senderDisplayName, date: millis.toDate(), text: text)
            msg.id = id
            msg.author = username
            completion(msg)
        })
    }
}
