//
//  CategoryPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol CategoryView: CommonView {
    func categoryFillData()
    func added()
}

protocol CategoryViewPresenter {
    var view: CategoryView {get set}
    init(view: CategoryView)
    func getAllCategory(completion: @escaping () -> ())
    func getUserCategory()
    func add(categoryDescriptions:[String])
    func add(categories: [Category])
    func delete(categoryDescription:String)
    func delete(category:Category)
}

class CategoryPresenter: CategoryViewPresenter {

    unowned var view: CategoryView
    var categories: [Category] = []
    var userCategories: [Category] = []
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    let categoryService: CategoryServices = CategoryServicesCenter()
    let userCategoryService: UserCategoryServices = UserCategoryServicesCenter()
    required init(view: CategoryView) {
        self.view = view
    }
    
    func getAllCategory(completion: @escaping () -> ()) {
        view.showLoading()
        categoryService.get(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                completion()
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let categories):
                strongSelf.categories = categories
                strongSelf.view.categoryFillData()
                strongSelf.getUserCategory()
                completion()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
                completion()
            }
        }
    }
    func getUserCategory() {
        view.showLoading()
        userCategoryService.get(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let categories):
                strongSelf.userCategories = categories
                strongSelf.view.categoryFillData()
            case .failure(let error):
                if error.code == 400 {
                    return
                } else {
                    strongSelf.view.showAlert(error.localizedDescription)
                }
            }
        }
    }
    func add(categoryDescriptions:[String]) {
        let categories = self.categories.filter({ (category) -> Bool in
            return categoryDescriptions.contains(category.description)
        })
        
        add(categories: categories)
    }
    
    func add(categories: [Category]) {
        view.showLoading()
        userCategoryService.add(categories: categories, username: username ) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let categories):
                strongSelf.userCategories = categories
                strongSelf.view.added()
                strongSelf.view.categoryFillData()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    func delete(categoryDescription:String) {
        guard let category = categories.filter({ (category) -> Bool in
            return category.description == categoryDescription
        }).first else {
            return
        }
        delete(category: category)
    }
    func delete(category: Category) {
        guard let index = userCategories.index(where: { (index) -> Bool in
            return index.id == category.id
        }) else {
            return
        }
        view.showLoading()
        userCategoryService.delete(category: category, username: username ) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let success):
                strongSelf.userCategories.remove(at: index)
                strongSelf.view.categoryFillData()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
}
