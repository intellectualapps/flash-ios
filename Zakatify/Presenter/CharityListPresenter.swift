//
//  CharityListPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol CharityListViewPresenter: TableViewPresenter {
    var charities: Variable<[CharityDetail]> {get set}
    var zakatifierSearchPresenter: ListZakatifiersPresenter? {get set}
    var view:TableView {get}
    init(view: TableView)
    func charity(index:Int) -> CharityDetail?
    func add(charity:CharityDetail)
    func remove(charity:CharityDetail)
    func loadmoreSearch(keyword: String?)
}

class CharityListPresenter: CharityListViewPresenter {
    var charities: Variable<[CharityDetail]> = Variable([CharityDetail]())
    var zakatifierSearchPresenter: ListZakatifiersPresenter?
    unowned var view: TableView
    var pageCount: Page = PageCount()
    var pageSearch: Page = PageCount()
    let service: CharityServices = CharityServicesCenter()
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    var charityViewModel = CharityViewModel()
    required init(view: TableView) {
        self.view = view
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.added.rawValue),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.remove.rawValue),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return charities.value.count
    }
    
    @objc func notificationChange() {
        view.refreshed()
    }
    
    var searchCancelable: Cancellable?
    func refresh(search: String? = nil) {
        guard let search = search else {
            return
        }
        if search.isEmpty {
            return
        }
        if searchCancelable?.cancel() != nil {
            view.refreshed()
        }
        view.refreshing()
        charityViewModel.charitySearch(username: username, keyword: search, page: pageSearch.page, size: pageSearch.size) {  [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let dict = response as? [String:Any] {
                    if let charitiesInfo = dict["charities"] as? CharitiesInfo {
                        strongSelf.charities.value = charitiesInfo.charities
                        strongSelf.pageSearch = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                        if search.count > 0 {
                            let params: AppEvent.ParametersDictionary = [
                                AppEventParameterName.custom("Search term"): search,
                                AppEventParameterName.custom("Charities_Results_number"): String(charitiesInfo.charities.count),
                                AppEventParameterName.custom("Total_Results"): String(charitiesInfo.charities.count)
                            ]
                            UIViewController.logEvent(eventName: .search, parameters: params)
                        }
                    }
                    strongSelf.zakatifierSearchPresenter?.zakatifiers = []
                    if let zakatifiersInfo = dict["zakatifiers"] as? [ZakatifierInfo] {
                        for zakatifierInfo in zakatifiersInfo {
                            if let zakaProfile = UserPublicProfile(JSON: [:]) {
                                zakaProfile.user = zakatifierInfo
                                strongSelf.zakatifierSearchPresenter?.zakatifiers.append(zakaProfile)
                            }
                        }
                    }
                }
                strongSelf.view.refreshed()
            }
        }
        
        /*searchCancelable = service.search(username: username, key: search, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                strongSelf.charities.value = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                if search.count > 0 {
                    let params: AppEvent.ParametersDictionary = [
                        AppEventParameterName.custom("Search term"): search,
                        AppEventParameterName.custom("Charities_Results_number"): String(charities.count),
                        AppEventParameterName.custom("Total_Results"): String(charities.count)
                    ]
                    UIViewController.logEvent(eventName: .search, parameters: params)
                }
            case .failure(let error):
                if error.code == 4  || error.code == 200 {
                    return
                }

                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.charities.value = []
            }
            strongSelf.view.refreshed()
        }*/
    }
    
    func refresh() {
        view.refreshing()
        charityViewModel.getCharitiesListl(username: username, page: 1, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let charitiesInfo = response as? CharitiesInfo {
                    strongSelf.charities.value = charitiesInfo.charities
                    strongSelf.pageCount = PageCount(page: 1, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.view.refreshed()
            }
        }
        
        /*service.suggestList(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                strongSelf.charities.value = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                if error.code == 200 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }*/
    }
    
    func loadmoreSearch(keyword: String?) {
        guard let search = keyword else {
            return
        }
        if search.isEmpty {
            return
        }
        if searchCancelable?.cancel() != nil {
            view.refreshed()
        }
        view.refreshing()
        charityViewModel.charitySearch(username: username, keyword: search, page: pageSearch.page + 1, size: pageSearch.size) {  [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                var tmp = strongSelf.charities.value
                if let charitiesInfo = response as? CharitiesInfo {
                    let _charities = charitiesInfo.charities
                    tmp.append(contentsOf: _charities)
                    strongSelf.pageSearch = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.charities.value = tmp
                strongSelf.view.refreshed()
            }
        }
    }
    
    func loadmore() {
        if shouldLoadmore() == false {
            return
        }
        view.refreshing()
        charityViewModel.getCharitiesListl(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                var tmp = strongSelf.charities.value
                if let charitiesInfo = response as? CharitiesInfo {
                    if let _charities = charitiesInfo.charities as? [CharityDetail] {
                        tmp.append(contentsOf: _charities)
                    }
                    strongSelf.pageCount = PageCount(page: charitiesInfo.pageNumber, size: charitiesInfo.pageSize, total: charitiesInfo.totalCount)
                }
                strongSelf.charities.value = tmp
                strongSelf.view.refreshed()
            }
        }
        
        /*service.suggestList(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                var tmp = strongSelf.charities.value
                if charities.count > 0 , let _charities = charities as? [CharityDetail] {
                    tmp.append(contentsOf: _charities)
                    strongSelf.pageCount = PageCount(page: page, size: size, total: total)
                }
                strongSelf.charities.value = tmp
                
            case .failure(let error):
                if error.code == 200 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }*/
    }
    
    func shouldLoadmore() -> Bool {
        return true
    }
    
    func charity(index: Int) -> CharityDetail? {
        if index < charities.value.count {
            return charities.value[index]
        }
        return nil
    }
    
    func add(charity:CharityDetail) {
        let index = charities.value.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.addToPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                if let index = index {
                    self?.charities.value[index].added = true
                }
                strongSelf.view.refreshed()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
    func remove(charity:CharityDetail) {
        let index = charities.value.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.removeFromPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                if let index = index {
                    self?.charities.value[index].added = false
                    self?.charities.value.remove(at: index)
                }
                strongSelf.view.refreshed()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
}
