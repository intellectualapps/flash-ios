//
//  CreditCardPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

enum PaymentType: String {
    case paypal = "PP"
    case card = "CC"
}
protocol CreditCardView: TableView {
    func saveSuccess()
}

protocol CreditCardViewPresenter: TableViewPresenter {
    var view: CreditCardView {get set}
    init(view: CreditCardView)
    func paymentAt(indexPath:IndexPath) -> Payment?
    func add(type: PaymentType, digit: Int)
    func delete(index:IndexPath)
    func makePrimary(index:IndexPath)
}

class CreditCardPresenter: CreditCardViewPresenter {
    let paymentModel = PaymentViewModel()
    var primary_default = 0
    unowned var view: CreditCardView
    let service: PaymentServices = PaymentServicesCenter()
    var payments:[Payment] = []
    var username: String {
        return UserManager.shared.currentUser?.username ?? ""
    }

    required init(view: CreditCardView) {
        self.view = view
        refresh()
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section:Int) -> Int {
        return payments.count
    }
    
    func paymentAt(indexPath:IndexPath) -> Payment? {
        if indexPath.row < payments.count {
            return payments[indexPath.row]
        }
        return nil
    }
    
    func add(type: PaymentType, digit: Int) {
        view.showLoading()
        paymentModel.addPaymenOption(username: username, primary: primary_default, type: type, digit: digit) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let state = response as? Bool , state {
                    strongSelf.primary_default = 0
                    strongSelf.view.saveSuccess()
                }else {
                    strongSelf.view.showAlert("Add error")
                }
            }
        }
        /*service.add(payment: payment, username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let payments):
                strongSelf.payments = payments
                strongSelf.view.saveSuccess()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }*/
    }
    
    func delete(index: IndexPath) {
        if index.row >= payments.count {
            return
        }
        let payment = payments[index.row]
        view.showLoading()
        paymentModel.deleteOption(payment: payment, username: username) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let state = response as? Bool , state {
                    strongSelf.payments.remove(at: index.row)
                    strongSelf.view.saveSuccess()
                }else {
                    strongSelf.view.showAlert("Delete error")
                }
            }
        }
        
        /*service.delete(payment: payment, username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let succes):
                strongSelf.payments.remove(at: index.row)
                strongSelf.view.saveSuccess()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }*/
    }
    
    func makePrimary(index: IndexPath) {
        if index.row >= payments.count {
            return
        }
        let payment = payments[index.row]
        let currentPaymentPrimary: Payment? = payments.filter { (payment) -> Bool in
            payment.isPrimary == 1
        }.first
        view.showLoading()
        paymentModel.setPrimary(payment: payment) { [weak self] (response, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let err = error {
                strongSelf.view.showAlert(err.message)
            } else {
                if let state = response as? Bool, state {
                    payment.isPrimary = 1
                    currentPaymentPrimary?.isPrimary = 0
                    strongSelf.view.saveSuccess()
                } else {
                    strongSelf.view.showAlert("Make primary error")
                }
            }
        }
    }
    
    func refresh(search: String?) {
        view.refreshing()
        paymentModel.getPaypalsAccount {[weak self] (accounts) in
            guard let `self` = self else {
                return
            }
            var temp = [Payment]()
            accounts.forEach({ (account) in
                temp.append(account.paymentInfo())
            })
            self.payments = temp
            self.view.refreshed()
        }
//        view.refreshing()
//        service.get(username: username) { [weak self] (result) in
//            guard let strongSelf = self else {
//                return
//            }
//            switch result {
//            case .success(let payments):
//                strongSelf.payments = payments
//                strongSelf.view.refreshed()
//            case .failure(let error):
//                if error.code == 400 {
//                    strongSelf.view.refreshed()
//                    return
//                } else {
//                    strongSelf.view.showAlert(error.localizedDescription)
//                }
//                strongSelf.view.refreshed()
//            }
//        }
    }
    
    func refresh() {

        self.refresh(search: nil)
    }
    
    func loadmore() {
        
    }
    func shouldLoadmore() -> Bool {
        return false
    }
}
